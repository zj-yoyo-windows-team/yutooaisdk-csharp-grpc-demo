// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: client/PrivateInterface.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021, 8981
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace YoYo.YuTooAiPb {

  /// <summary>Holder for reflection information generated from client/PrivateInterface.proto</summary>
  public static partial class PrivateInterfaceReflection {

    #region Descriptor
    /// <summary>File descriptor for client/PrivateInterface.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static PrivateInterfaceReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Ch1jbGllbnQvUHJpdmF0ZUludGVyZmFjZS5wcm90bxIRWW9Zby5ZdVRvb0Fp",
            "UGIuVjEaGXNoYXJlZC9TaGFyZWREZWZpbmUucHJvdG8aGnNoYXJlZC9Qcml2",
            "YXRlRGVmaW5lLnByb3RvMsEHCg1Zb1lvQWlQcml2YXRlElIKCVNldEtleVZh",
            "bBIjLllvWW8uWXVUb29BaVBiLlYxLlNldEtleVZhbFJlcXVlc3QaHi5Zb1lv",
            "Lll1VG9vQWlQYi5WMS5QdWJsaWNSZXBseSIAElUKCUdldEtleVZhbBIjLllv",
            "WW8uWXVUb29BaVBiLlYxLkdldEtleVZhbFJlcXVlc3QaIS5Zb1lvLll1VG9v",
            "QWlQYi5WMS5HZXRLZXlWYWxSZXBseSIAEl0KDlNjYWxlVHJhbnNwb3J0Eh8u",
            "WW9Zby5ZdVRvb0FpUGIuVjEuRW1wdHlSZXF1ZXN0GiYuWW9Zby5ZdVRvb0Fp",
            "UGIuVjEuU2NhbGVUcmFuc3BvcnRSZXBseSIAMAESZwoPR2V0SWRlbnRpZnlJ",
            "bmZvEikuWW9Zby5ZdVRvb0FpUGIuVjEuR2V0SWRlbnRpZnlJbmZvUmVxdWVz",
            "dBonLllvWW8uWXVUb29BaVBiLlYxLkdldElkZW50aWZ5SW5mb1JlcGx5IgAS",
            "VAoPVXBsb2FkU3R1ZHlEYXRhEh8uWW9Zby5ZdVRvb0FpUGIuVjEuRW1wdHlS",
            "ZXF1ZXN0Gh4uWW9Zby5ZdVRvb0FpUGIuVjEuUHVibGljUmVwbHkiABJhChFH",
            "ZXRBY3RpdmF0ZVFyQ29kZRIfLllvWW8uWXVUb29BaVBiLlYxLkVtcHR5UmVx",
            "dWVzdBopLllvWW8uWXVUb29BaVBiLlYxLkdldEFjdGl2YXRlUXJDb2RlUmVw",
            "bHkiABJYChNGbHVzaEFjdGl2YXRlU3RhdHVzEh8uWW9Zby5ZdVRvb0FpUGIu",
            "VjEuRW1wdHlSZXF1ZXN0Gh4uWW9Zby5ZdVRvb0FpUGIuVjEuUHVibGljUmVw",
            "bHkiABJeCgxRdWVyeVBsdUZlYXQSJi5Zb1lvLll1VG9vQWlQYi5WMS5RdWVy",
            "eVBsdUZlYXRSZXF1ZXN0GiQuWW9Zby5ZdVRvb0FpUGIuVjEuUXVlcnlQbHVG",
            "ZWF0UmVwbHkiABJwChJVcGxvYWRDbG91ZFN0b3JhZ2USLC5Zb1lvLll1VG9v",
            "QWlQYi5WMS5VcGxvYWRDbG91ZFN0b3JhZ2VSZXF1ZXN0GiouWW9Zby5ZdVRv",
            "b0FpUGIuVjEuVXBsb2FkQ2xvdWRTdG9yYWdlUmVwbHkiABJYCgxTZW5kSGlk",
            "RXZlbnQSJi5Zb1lvLll1VG9vQWlQYi5WMS5TZW5kSGlkRXZlbnRSZXF1ZXN0",
            "Gh4uWW9Zby5ZdVRvb0FpUGIuVjEuUHVibGljUmVwbHkiAEI1ChNsaW5rLnlv",
            "eW8ueXV0b29haXBiUAFaCy47eXV0b29haXBiqgIOWW9Zby5ZdVRvb0FpUGJi",
            "BnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::YoYo.YuTooAiPb.SharedDefineReflection.Descriptor, global::YoYo.YuTooAiPb.PrivateDefineReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, null));
    }
    #endregion

  }
}

#endregion Designer generated code
