﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using YoYo.YuTooAiPb;

namespace YoYo.YuTooAiSDK.Demo
{
    public partial class StressForm : Form
    {
        AiGrpc sdk;
        AiLogger log;
        BlockingCollection<AiMatchingRequest> aiMatchingReqQueue = new BlockingCollection<AiMatchingRequest>();

        bool running = false;

        double minTime;
        double maxTime;
        double avgTime;
        long countTotal;
        long countFail;

        public StressForm(ref AiGrpc grpc, ref AiLogger logger)
        {
            sdk = grpc;
            log = logger;
            InitializeComponent();
        }

        private void UpdateLabelText(string s)
        {
            if (label1.InvokeRequired)
            {
                Action<string> action = new Action<string>(UpdateLabelText);
                Invoke(action, new object[] { s });
            }
            else
            {
                label1.Text = s;
            }
        }

        private void UpdateImage(Image img)
        {
            if (pictureBox1.InvokeRequired)
            {
                Action<Image> action = new Action<Image>(UpdateImage);
                Invoke(action, new object[] { img });
            }
            else
            {
                pictureBox1.Image = img;
            }
        }

        private void StressForm_Load(object sender, EventArgs e)
        {
            label1.Text = "Min:0ms Max:0ms Avg:0ms Total:0 Failed:0";
        }

        double startTime;
        private void button1_Click(object sender, EventArgs e)
        {
            if (running)
            {
                return;
            }

            running = true;
            avgTime = minTime = maxTime = countFail = countTotal = 0;
            sdk.AiMatchingStream(aiMatchingReqQueue, (reply) =>
            {
                try
                {
                    var nowTime = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                    var currTime = nowTime - startTime;
                    if (countTotal == 0)
                    {
                        avgTime = minTime = maxTime = currTime;
                    }
                    else
                    {
                        if (avgTime < minTime)
                        {
                            minTime = currTime;
                        }
                        else if (avgTime > maxTime)
                        {
                            maxTime = currTime;
                        }
                        avgTime = (currTime + avgTime) / 2;
                    }
                    countTotal += 1;

                    log.Info($"--------{currTime:f4}ms");

                    if (reply == null || reply.Status == null)
                    {
                        countFail += 1;
                        log.Error("AiMatchingStream回调了空结果");
                    }
                    else if (reply.Status.Code != ReplyCodeType.Success)
                    {
                        countFail += 1;
                        log.Error($"识别异常：{reply.Status.Msg}");
                    }
                    else
                    {
                        log.Info($"识别成功：{reply.Status.Msg}");
                        log.Info($"识别请求入参：{reply.AiMatchingRequest}");
                        log.Info($"识别ID：{reply.IdentifyID}");
                        log.Info($"识别结果：{reply.PluList.Count}个");
                        int index = 0;
                        foreach (var item in reply.PluList)
                        {
                            index++;
                            log.Info($"{index}、 {item.Code} | {item.Name}");
                        }
                    }

                    UpdateLabelText($"Min:{minTime:f4}ms Max:{maxTime:f4}ms Avg:{avgTime:f4}ms Total:{countTotal} Failed:{countFail}");
                    var imageReply = sdk.GetCameraImage();
                    using (Stream memStream = new MemoryStream(imageReply.ImgData.ToByteArray()))
                    {
                        UpdateImage(Image.FromStream(memStream));
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }
                finally
                {
                    log.Info("--------");
                }
                return true;
            });

            // 异步开刷
            Task.Run(() =>
            {
                while (running)
                {
                    startTime = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                    aiMatchingReqQueue?.Add(new AiMatchingRequest
                    {
                        MaxNum = 5,
                        MatchType = MatchingType.Forcibly,
                        Weight = 100,
                        Stable = WeightStatusType.Stable
                    });
                    Thread.Sleep(300);
                }
            });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            running = false;
        }

        private void StressForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            running = false;
        }
    }
}
