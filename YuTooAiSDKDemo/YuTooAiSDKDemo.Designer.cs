﻿namespace YoYo.YuTooAiSDK.Demo
{
    partial class YuTooAiSDKDemo
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YuTooAiSDKDemo));
            this.BtnAiServerRun = new System.Windows.Forms.Button();
            this.BtnShowCameraAreaView = new System.Windows.Forms.Button();
            this.VendorInfoBox = new System.Windows.Forms.Panel();
            this.VendorKeyBox = new System.Windows.Forms.Panel();
            this.VendorKeyInput = new System.Windows.Forms.TextBox();
            this.VendorCodeBox = new System.Windows.Forms.Panel();
            this.VendorCodeInput = new System.Windows.Forms.TextBox();
            this.VendorKeyLabel = new System.Windows.Forms.Label();
            this.VendorCodeLabel = new System.Windows.Forms.Label();
            this.ActivateInfoBox = new System.Windows.Forms.Panel();
            this.PhoneBox = new System.Windows.Forms.Panel();
            this.PhoneInput = new System.Windows.Forms.TextBox();
            this.ContactBox = new System.Windows.Forms.Panel();
            this.ContactInput = new System.Windows.Forms.TextBox();
            this.SHopNameBox = new System.Windows.Forms.Panel();
            this.ShopNameInput = new System.Windows.Forms.TextBox();
            this.CdKeyBox = new System.Windows.Forms.Panel();
            this.CdKeyInput = new System.Windows.Forms.TextBox();
            this.PhoneLabel = new System.Windows.Forms.Label();
            this.ContactLabel = new System.Windows.Forms.Label();
            this.ShopNameLabel = new System.Windows.Forms.Label();
            this.CdKeyLabel = new System.Windows.Forms.Label();
            this.BtnDeviceActivate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LogPrintBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.BtnAiMark = new System.Windows.Forms.Button();
            this.BtnGetStudyData = new System.Windows.Forms.Button();
            this.BtnSetStudyData = new System.Windows.Forms.Button();
            this.BtnPluSync = new System.Windows.Forms.Button();
            this.BtnAiMatching = new System.Windows.Forms.Button();
            this.BtnAiMatchingStream = new System.Windows.Forms.Button();
            this.BtnStress = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.BtnGetDeviceInfo = new System.Windows.Forms.Button();
            this.BtnUnInit = new System.Windows.Forms.Button();
            this.WeightSimulationBox = new System.Windows.Forms.GroupBox();
            this.BtnInit = new System.Windows.Forms.Button();
            this.GrpcPortLabel = new System.Windows.Forms.Label();
            this.WeightSimulation = new YoYo.YuTooAiSDK.Demo.Compent.WeightSimulation();
            this.VendorInfoBox.SuspendLayout();
            this.VendorKeyBox.SuspendLayout();
            this.VendorCodeBox.SuspendLayout();
            this.ActivateInfoBox.SuspendLayout();
            this.PhoneBox.SuspendLayout();
            this.ContactBox.SuspendLayout();
            this.SHopNameBox.SuspendLayout();
            this.CdKeyBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.WeightSimulationBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnAiServerRun
            // 
            this.BtnAiServerRun.BackColor = System.Drawing.Color.MediumTurquoise;
            this.BtnAiServerRun.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnAiServerRun.Location = new System.Drawing.Point(755, 213);
            this.BtnAiServerRun.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnAiServerRun.Name = "BtnAiServerRun";
            this.BtnAiServerRun.Size = new System.Drawing.Size(100, 40);
            this.BtnAiServerRun.TabIndex = 0;
            this.BtnAiServerRun.Text = "服务启动";
            this.BtnAiServerRun.UseVisualStyleBackColor = false;
            this.BtnAiServerRun.Click += new System.EventHandler(this.BtnAiServerRun_Click);
            // 
            // BtnShowCameraAreaView
            // 
            this.BtnShowCameraAreaView.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnShowCameraAreaView.Location = new System.Drawing.Point(5, 6);
            this.BtnShowCameraAreaView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnShowCameraAreaView.Name = "BtnShowCameraAreaView";
            this.BtnShowCameraAreaView.Size = new System.Drawing.Size(138, 35);
            this.BtnShowCameraAreaView.TabIndex = 1;
            this.BtnShowCameraAreaView.Text = "调整识别区域";
            this.BtnShowCameraAreaView.UseVisualStyleBackColor = true;
            this.BtnShowCameraAreaView.Click += new System.EventHandler(this.BtnShowCameraAreaView_Click);
            // 
            // VendorInfoBox
            // 
            this.VendorInfoBox.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.VendorInfoBox.Controls.Add(this.VendorKeyBox);
            this.VendorInfoBox.Controls.Add(this.VendorCodeBox);
            this.VendorInfoBox.Controls.Add(this.VendorKeyLabel);
            this.VendorInfoBox.Controls.Add(this.VendorCodeLabel);
            this.VendorInfoBox.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VendorInfoBox.Location = new System.Drawing.Point(14, 17);
            this.VendorInfoBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VendorInfoBox.Name = "VendorInfoBox";
            this.VendorInfoBox.Size = new System.Drawing.Size(947, 50);
            this.VendorInfoBox.TabIndex = 4;
            // 
            // VendorKeyBox
            // 
            this.VendorKeyBox.BackColor = System.Drawing.Color.White;
            this.VendorKeyBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VendorKeyBox.Controls.Add(this.VendorKeyInput);
            this.VendorKeyBox.Location = new System.Drawing.Point(565, 8);
            this.VendorKeyBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VendorKeyBox.Name = "VendorKeyBox";
            this.VendorKeyBox.Size = new System.Drawing.Size(369, 35);
            this.VendorKeyBox.TabIndex = 9;
            // 
            // VendorKeyInput
            // 
            this.VendorKeyInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.VendorKeyInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VendorKeyInput.Location = new System.Drawing.Point(7, 7);
            this.VendorKeyInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VendorKeyInput.Name = "VendorKeyInput";
            this.VendorKeyInput.Size = new System.Drawing.Size(353, 17);
            this.VendorKeyInput.TabIndex = 4;
            this.VendorKeyInput.Tag = "";
            this.VendorKeyInput.WordWrap = false;
            // 
            // VendorCodeBox
            // 
            this.VendorCodeBox.BackColor = System.Drawing.Color.White;
            this.VendorCodeBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VendorCodeBox.Controls.Add(this.VendorCodeInput);
            this.VendorCodeBox.Location = new System.Drawing.Point(96, 8);
            this.VendorCodeBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VendorCodeBox.Name = "VendorCodeBox";
            this.VendorCodeBox.Size = new System.Drawing.Size(366, 35);
            this.VendorCodeBox.TabIndex = 8;
            // 
            // VendorCodeInput
            // 
            this.VendorCodeInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.VendorCodeInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VendorCodeInput.Location = new System.Drawing.Point(7, 7);
            this.VendorCodeInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VendorCodeInput.Name = "VendorCodeInput";
            this.VendorCodeInput.Size = new System.Drawing.Size(350, 17);
            this.VendorCodeInput.TabIndex = 4;
            this.VendorCodeInput.WordWrap = false;
            // 
            // VendorKeyLabel
            // 
            this.VendorKeyLabel.AutoSize = true;
            this.VendorKeyLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VendorKeyLabel.Location = new System.Drawing.Point(481, 15);
            this.VendorKeyLabel.Name = "VendorKeyLabel";
            this.VendorKeyLabel.Size = new System.Drawing.Size(69, 19);
            this.VendorKeyLabel.TabIndex = 7;
            this.VendorKeyLabel.Text = "厂商密钥:";
            this.VendorKeyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VendorCodeLabel
            // 
            this.VendorCodeLabel.AutoSize = true;
            this.VendorCodeLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VendorCodeLabel.Location = new System.Drawing.Point(12, 15);
            this.VendorCodeLabel.Name = "VendorCodeLabel";
            this.VendorCodeLabel.Size = new System.Drawing.Size(69, 19);
            this.VendorCodeLabel.TabIndex = 5;
            this.VendorCodeLabel.Text = "厂商标识:";
            this.VendorCodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ActivateInfoBox
            // 
            this.ActivateInfoBox.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ActivateInfoBox.Controls.Add(this.PhoneBox);
            this.ActivateInfoBox.Controls.Add(this.ContactBox);
            this.ActivateInfoBox.Controls.Add(this.SHopNameBox);
            this.ActivateInfoBox.Controls.Add(this.CdKeyBox);
            this.ActivateInfoBox.Controls.Add(this.PhoneLabel);
            this.ActivateInfoBox.Controls.Add(this.ContactLabel);
            this.ActivateInfoBox.Controls.Add(this.ShopNameLabel);
            this.ActivateInfoBox.Controls.Add(this.CdKeyLabel);
            this.ActivateInfoBox.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ActivateInfoBox.Location = new System.Drawing.Point(14, 76);
            this.ActivateInfoBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ActivateInfoBox.Name = "ActivateInfoBox";
            this.ActivateInfoBox.Size = new System.Drawing.Size(947, 50);
            this.ActivateInfoBox.TabIndex = 5;
            // 
            // PhoneBox
            // 
            this.PhoneBox.BackColor = System.Drawing.Color.White;
            this.PhoneBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PhoneBox.Controls.Add(this.PhoneInput);
            this.PhoneBox.Location = new System.Drawing.Point(797, 8);
            this.PhoneBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PhoneBox.Name = "PhoneBox";
            this.PhoneBox.Size = new System.Drawing.Size(137, 35);
            this.PhoneBox.TabIndex = 16;
            // 
            // PhoneInput
            // 
            this.PhoneInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PhoneInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PhoneInput.Location = new System.Drawing.Point(5, 7);
            this.PhoneInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PhoneInput.Name = "PhoneInput";
            this.PhoneInput.Size = new System.Drawing.Size(125, 17);
            this.PhoneInput.TabIndex = 4;
            this.PhoneInput.WordWrap = false;
            // 
            // ContactBox
            // 
            this.ContactBox.BackColor = System.Drawing.Color.White;
            this.ContactBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContactBox.Controls.Add(this.ContactInput);
            this.ContactBox.Location = new System.Drawing.Point(565, 8);
            this.ContactBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ContactBox.Name = "ContactBox";
            this.ContactBox.Size = new System.Drawing.Size(137, 35);
            this.ContactBox.TabIndex = 15;
            // 
            // ContactInput
            // 
            this.ContactInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ContactInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ContactInput.Location = new System.Drawing.Point(5, 7);
            this.ContactInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ContactInput.Name = "ContactInput";
            this.ContactInput.Size = new System.Drawing.Size(125, 17);
            this.ContactInput.TabIndex = 4;
            this.ContactInput.WordWrap = false;
            // 
            // SHopNameBox
            // 
            this.SHopNameBox.BackColor = System.Drawing.Color.White;
            this.SHopNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SHopNameBox.Controls.Add(this.ShopNameInput);
            this.SHopNameBox.Location = new System.Drawing.Point(325, 8);
            this.SHopNameBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SHopNameBox.Name = "SHopNameBox";
            this.SHopNameBox.Size = new System.Drawing.Size(137, 35);
            this.SHopNameBox.TabIndex = 14;
            // 
            // ShopNameInput
            // 
            this.ShopNameInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ShopNameInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ShopNameInput.Location = new System.Drawing.Point(5, 7);
            this.ShopNameInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ShopNameInput.Name = "ShopNameInput";
            this.ShopNameInput.Size = new System.Drawing.Size(125, 17);
            this.ShopNameInput.TabIndex = 4;
            // 
            // CdKeyBox
            // 
            this.CdKeyBox.BackColor = System.Drawing.Color.White;
            this.CdKeyBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CdKeyBox.Controls.Add(this.CdKeyInput);
            this.CdKeyBox.Location = new System.Drawing.Point(96, 8);
            this.CdKeyBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CdKeyBox.Name = "CdKeyBox";
            this.CdKeyBox.Size = new System.Drawing.Size(137, 35);
            this.CdKeyBox.TabIndex = 13;
            // 
            // CdKeyInput
            // 
            this.CdKeyInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CdKeyInput.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CdKeyInput.Location = new System.Drawing.Point(5, 7);
            this.CdKeyInput.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CdKeyInput.Name = "CdKeyInput";
            this.CdKeyInput.Size = new System.Drawing.Size(125, 17);
            this.CdKeyInput.TabIndex = 4;
            this.CdKeyInput.WordWrap = false;
            // 
            // PhoneLabel
            // 
            this.PhoneLabel.AutoSize = true;
            this.PhoneLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PhoneLabel.Location = new System.Drawing.Point(713, 16);
            this.PhoneLabel.Name = "PhoneLabel";
            this.PhoneLabel.Size = new System.Drawing.Size(69, 19);
            this.PhoneLabel.TabIndex = 12;
            this.PhoneLabel.Text = "联系电话:";
            // 
            // ContactLabel
            // 
            this.ContactLabel.AutoSize = true;
            this.ContactLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ContactLabel.Location = new System.Drawing.Point(493, 16);
            this.ContactLabel.Name = "ContactLabel";
            this.ContactLabel.Size = new System.Drawing.Size(55, 19);
            this.ContactLabel.TabIndex = 10;
            this.ContactLabel.Text = "联系人:";
            // 
            // ShopNameLabel
            // 
            this.ShopNameLabel.AutoSize = true;
            this.ShopNameLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ShopNameLabel.Location = new System.Drawing.Point(240, 15);
            this.ShopNameLabel.Name = "ShopNameLabel";
            this.ShopNameLabel.Size = new System.Drawing.Size(69, 19);
            this.ShopNameLabel.TabIndex = 8;
            this.ShopNameLabel.Text = "门店名称:";
            // 
            // CdKeyLabel
            // 
            this.CdKeyLabel.AutoSize = true;
            this.CdKeyLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CdKeyLabel.Location = new System.Drawing.Point(12, 15);
            this.CdKeyLabel.Name = "CdKeyLabel";
            this.CdKeyLabel.Size = new System.Drawing.Size(69, 19);
            this.CdKeyLabel.TabIndex = 6;
            this.CdKeyLabel.Text = "激活密钥:";
            // 
            // BtnDeviceActivate
            // 
            this.BtnDeviceActivate.BackColor = System.Drawing.Color.PaleGreen;
            this.BtnDeviceActivate.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnDeviceActivate.Location = new System.Drawing.Point(861, 213);
            this.BtnDeviceActivate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnDeviceActivate.Name = "BtnDeviceActivate";
            this.BtnDeviceActivate.Size = new System.Drawing.Size(100, 40);
            this.BtnDeviceActivate.TabIndex = 6;
            this.BtnDeviceActivate.Text = "设备激活";
            this.BtnDeviceActivate.UseVisualStyleBackColor = false;
            this.BtnDeviceActivate.Click += new System.EventHandler(this.BtnDeviceActivate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LogPrintBox);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox1.Location = new System.Drawing.Point(12, 307);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(951, 250);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "日志";
            // 
            // LogPrintBox
            // 
            this.LogPrintBox.BackColor = System.Drawing.SystemColors.Menu;
            this.LogPrintBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LogPrintBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LogPrintBox.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LogPrintBox.Location = new System.Drawing.Point(3, 24);
            this.LogPrintBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LogPrintBox.Multiline = true;
            this.LogPrintBox.Name = "LogPrintBox";
            this.LogPrintBox.ReadOnly = true;
            this.LogPrintBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.LogPrintBox.Size = new System.Drawing.Size(945, 222);
            this.LogPrintBox.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.AliceBlue;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.BtnAiMark, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.BtnGetStudyData, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnSetStudyData, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnShowCameraAreaView, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.BtnPluSync, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.BtnAiMatching, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.BtnAiMatchingStream, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.BtnStress, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.button7, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnGetDeviceInfo, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(14, 210);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(736, 92);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // BtnAiMark
            // 
            this.BtnAiMark.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnAiMark.Location = new System.Drawing.Point(589, 6);
            this.BtnAiMark.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnAiMark.Name = "BtnAiMark";
            this.BtnAiMark.Size = new System.Drawing.Size(142, 35);
            this.BtnAiMark.TabIndex = 9;
            this.BtnAiMark.Text = "AI标记";
            this.BtnAiMark.UseVisualStyleBackColor = true;
            this.BtnAiMark.Click += new System.EventHandler(this.BtnAiMark_Click);
            // 
            // BtnGetStudyData
            // 
            this.BtnGetStudyData.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnGetStudyData.Location = new System.Drawing.Point(151, 51);
            this.BtnGetStudyData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnGetStudyData.Name = "BtnGetStudyData";
            this.BtnGetStudyData.Size = new System.Drawing.Size(138, 35);
            this.BtnGetStudyData.TabIndex = 3;
            this.BtnGetStudyData.Text = "获取学习数据";
            this.BtnGetStudyData.UseVisualStyleBackColor = true;
            this.BtnGetStudyData.Click += new System.EventHandler(this.BtnGetStudyData_Click);
            // 
            // BtnSetStudyData
            // 
            this.BtnSetStudyData.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnSetStudyData.Location = new System.Drawing.Point(297, 51);
            this.BtnSetStudyData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnSetStudyData.Name = "BtnSetStudyData";
            this.BtnSetStudyData.Size = new System.Drawing.Size(138, 35);
            this.BtnSetStudyData.TabIndex = 2;
            this.BtnSetStudyData.Text = "配置学习数据";
            this.BtnSetStudyData.UseVisualStyleBackColor = true;
            this.BtnSetStudyData.Click += new System.EventHandler(this.BtnSetStudyData_Click);
            // 
            // BtnPluSync
            // 
            this.BtnPluSync.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnPluSync.Location = new System.Drawing.Point(151, 6);
            this.BtnPluSync.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnPluSync.Name = "BtnPluSync";
            this.BtnPluSync.Size = new System.Drawing.Size(138, 35);
            this.BtnPluSync.TabIndex = 7;
            this.BtnPluSync.Text = "同步商品数据";
            this.BtnPluSync.UseVisualStyleBackColor = true;
            this.BtnPluSync.Click += new System.EventHandler(this.BtnPluSync_Click);
            // 
            // BtnAiMatching
            // 
            this.BtnAiMatching.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnAiMatching.Location = new System.Drawing.Point(297, 6);
            this.BtnAiMatching.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnAiMatching.Name = "BtnAiMatching";
            this.BtnAiMatching.Size = new System.Drawing.Size(138, 35);
            this.BtnAiMatching.TabIndex = 8;
            this.BtnAiMatching.Text = "AI识别";
            this.BtnAiMatching.UseVisualStyleBackColor = true;
            this.BtnAiMatching.Click += new System.EventHandler(this.BtnAiMatching_Click);
            // 
            // BtnAiMatchingStream
            // 
            this.BtnAiMatchingStream.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnAiMatchingStream.Location = new System.Drawing.Point(443, 6);
            this.BtnAiMatchingStream.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnAiMatchingStream.Name = "BtnAiMatchingStream";
            this.BtnAiMatchingStream.Size = new System.Drawing.Size(138, 35);
            this.BtnAiMatchingStream.TabIndex = 4;
            this.BtnAiMatchingStream.Text = "AI识别（流式）";
            this.BtnAiMatchingStream.UseVisualStyleBackColor = true;
            this.BtnAiMatchingStream.Click += new System.EventHandler(this.BtnAiMatchingStream_Click);
            // 
            // BtnStress
            // 
            this.BtnStress.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnStress.Location = new System.Drawing.Point(589, 51);
            this.BtnStress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnStress.Name = "BtnStress";
            this.BtnStress.Size = new System.Drawing.Size(142, 35);
            this.BtnStress.TabIndex = 6;
            this.BtnStress.Text = "压测";
            this.BtnStress.UseVisualStyleBackColor = true;
            this.BtnStress.Click += new System.EventHandler(this.BtnStress_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button7.Location = new System.Drawing.Point(443, 51);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(138, 35);
            this.button7.TabIndex = 5;
            this.button7.Text = "无";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // BtnGetDeviceInfo
            // 
            this.BtnGetDeviceInfo.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnGetDeviceInfo.Location = new System.Drawing.Point(5, 51);
            this.BtnGetDeviceInfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnGetDeviceInfo.Name = "BtnGetDeviceInfo";
            this.BtnGetDeviceInfo.Size = new System.Drawing.Size(138, 35);
            this.BtnGetDeviceInfo.TabIndex = 2;
            this.BtnGetDeviceInfo.Text = "获取设备信息";
            this.BtnGetDeviceInfo.UseVisualStyleBackColor = true;
            this.BtnGetDeviceInfo.Click += new System.EventHandler(this.BtnGetDeviceInfo_Click);
            // 
            // BtnUnInit
            // 
            this.BtnUnInit.BackColor = System.Drawing.Color.DarkSalmon;
            this.BtnUnInit.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnUnInit.Location = new System.Drawing.Point(861, 261);
            this.BtnUnInit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnUnInit.Name = "BtnUnInit";
            this.BtnUnInit.Size = new System.Drawing.Size(100, 40);
            this.BtnUnInit.TabIndex = 9;
            this.BtnUnInit.Text = "服务释放";
            this.BtnUnInit.UseVisualStyleBackColor = false;
            this.BtnUnInit.Click += new System.EventHandler(this.BtnUnInit_Click);
            // 
            // WeightSimulationBox
            // 
            this.WeightSimulationBox.Controls.Add(this.WeightSimulation);
            this.WeightSimulationBox.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WeightSimulationBox.Location = new System.Drawing.Point(16, 133);
            this.WeightSimulationBox.Name = "WeightSimulationBox";
            this.WeightSimulationBox.Size = new System.Drawing.Size(734, 70);
            this.WeightSimulationBox.TabIndex = 12;
            this.WeightSimulationBox.TabStop = false;
            this.WeightSimulationBox.Text = "重量模拟";
            // 
            // BtnInit
            // 
            this.BtnInit.BackColor = System.Drawing.Color.Moccasin;
            this.BtnInit.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnInit.Location = new System.Drawing.Point(755, 261);
            this.BtnInit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnInit.Name = "BtnInit";
            this.BtnInit.Size = new System.Drawing.Size(100, 40);
            this.BtnInit.TabIndex = 13;
            this.BtnInit.Text = "服务初始化";
            this.BtnInit.UseVisualStyleBackColor = false;
            this.BtnInit.Click += new System.EventHandler(this.BtnInit_Click);
            // 
            // GrpcPortLabel
            // 
            this.GrpcPortLabel.AutoSize = true;
            this.GrpcPortLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.GrpcPortLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this.GrpcPortLabel.Location = new System.Drawing.Point(813, 160);
            this.GrpcPortLabel.Name = "GrpcPortLabel";
            this.GrpcPortLabel.Size = new System.Drawing.Size(77, 20);
            this.GrpcPortLabel.TabIndex = 10;
            this.GrpcPortLabel.Text = "端口：null";
            this.GrpcPortLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WeightSimulation
            // 
            this.WeightSimulation.Location = new System.Drawing.Point(13, 23);
            this.WeightSimulation.Margin = new System.Windows.Forms.Padding(0);
            this.WeightSimulation.Name = "WeightSimulation";
            this.WeightSimulation.Size = new System.Drawing.Size(705, 38);
            this.WeightSimulation.TabIndex = 0;
            this.WeightSimulation.WeightPointMoved += new YoYo.YuTooAiSDK.Demo.Compent.WeightSimulation.WeightMovePointMovedEvent(this.WeightSimulation_WeightPointMoved);
            // 
            // YuTooAiSDKDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 561);
            this.Controls.Add(this.GrpcPortLabel);
            this.Controls.Add(this.BtnInit);
            this.Controls.Add(this.WeightSimulationBox);
            this.Controls.Add(this.BtnUnInit);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BtnDeviceActivate);
            this.Controls.Add(this.ActivateInfoBox);
            this.Controls.Add(this.VendorInfoBox);
            this.Controls.Add(this.BtnAiServerRun);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "YuTooAiSDKDemo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "由图智能识别SDK演示程序";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.YuTooAiSDKDemo_FormClosed);
            this.Load += new System.EventHandler(this.YuTooAiSDKDemo_Load);
            this.VendorInfoBox.ResumeLayout(false);
            this.VendorInfoBox.PerformLayout();
            this.VendorKeyBox.ResumeLayout(false);
            this.VendorKeyBox.PerformLayout();
            this.VendorCodeBox.ResumeLayout(false);
            this.VendorCodeBox.PerformLayout();
            this.ActivateInfoBox.ResumeLayout(false);
            this.ActivateInfoBox.PerformLayout();
            this.PhoneBox.ResumeLayout(false);
            this.PhoneBox.PerformLayout();
            this.ContactBox.ResumeLayout(false);
            this.ContactBox.PerformLayout();
            this.SHopNameBox.ResumeLayout(false);
            this.SHopNameBox.PerformLayout();
            this.CdKeyBox.ResumeLayout(false);
            this.CdKeyBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.WeightSimulationBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnAiServerRun;
        private System.Windows.Forms.Button BtnShowCameraAreaView;
        private System.Windows.Forms.Panel VendorInfoBox;
        private System.Windows.Forms.Label VendorCodeLabel;
        private System.Windows.Forms.TextBox VendorCodeInput;
        private System.Windows.Forms.Panel ActivateInfoBox;
        private System.Windows.Forms.Label CdKeyLabel;
        private System.Windows.Forms.Label ShopNameLabel;
        private System.Windows.Forms.Button BtnDeviceActivate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label VendorKeyLabel;
        private System.Windows.Forms.Label PhoneLabel;
        private System.Windows.Forms.Label ContactLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox LogPrintBox;
        private System.Windows.Forms.Button BtnAiMark;
        private System.Windows.Forms.Button BtnAiMatching;
        private System.Windows.Forms.Button BtnPluSync;
        private System.Windows.Forms.Button BtnGetDeviceInfo;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button BtnAiMatchingStream;
        private System.Windows.Forms.Button BtnGetStudyData;
        private System.Windows.Forms.Button BtnSetStudyData;
        private System.Windows.Forms.Button BtnUnInit;
        private System.Windows.Forms.Panel VendorKeyBox;
        private System.Windows.Forms.TextBox VendorKeyInput;
        private System.Windows.Forms.Panel VendorCodeBox;
        private System.Windows.Forms.Panel PhoneBox;
        private System.Windows.Forms.TextBox PhoneInput;
        private System.Windows.Forms.Panel ContactBox;
        private System.Windows.Forms.TextBox ContactInput;
        private System.Windows.Forms.Panel SHopNameBox;
        private System.Windows.Forms.TextBox ShopNameInput;
        private System.Windows.Forms.Panel CdKeyBox;
        private System.Windows.Forms.TextBox CdKeyInput;
        private System.Windows.Forms.GroupBox WeightSimulationBox;
        private System.Windows.Forms.Button BtnStress;
        private System.Windows.Forms.Button BtnInit;
        private Compent.WeightSimulation WeightSimulation;
        private System.Windows.Forms.Label GrpcPortLabel;
    }
}

