﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace YoYo.YuTooAiSDK.Demo.Compent
{
    /// <summary>
    /// 重量模拟背景条
    /// </summary>
    public partial class WeightBackBar : UserControl
    {
        public WeightBackBar()
        {
            InitializeComponent();
            // 初始化
            DrawBackgroundImage();
        }

        /// <summary>
        /// 绘制背景图片
        /// </summary>
        private void DrawBackgroundImage()
        {
            // 构建画布
            Bitmap bm = new Bitmap(Width, Height);
            Graphics g = Graphics.FromImage(bm);
            // 绘制单色
            SolidBrush brush = new SolidBrush(BackColor);
            FillRoundRectangle(g, brush, new Rectangle(0, 0, Width, Height), Height / 2);
            // 赋值到背景上
            BackgroundImage = bm;
            g.Dispose();
        }

        /// <summary>
        /// 绘制实心的圆角矩形
        /// </summary>
        /// <param name="g"></param>
        /// <param name="brush"></param>
        /// <param name="rect"></param>
        /// <param name="cornerRadius"></param>
        private void FillRoundRectangle(Graphics g, Brush brush, Rectangle rect, int cornerRadius)
        {
            using (GraphicsPath path = CreateRoundedRectanglePath(rect, cornerRadius))
            {
                Region = new Region(path);
                g.FillPath(brush, path);
            }
        }

        /// <summary>
        /// 创建圆角矩形路径
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="cornerRadius"></param>
        /// <returns></returns>
        private GraphicsPath CreateRoundedRectanglePath(Rectangle rect, int cornerRadius)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(rect.X, rect.Y, cornerRadius * 2, cornerRadius * 2, 180, 90);
            roundedRect.AddLine(rect.X + cornerRadius, rect.Y, rect.Right - cornerRadius * 2, rect.Y);
            roundedRect.AddArc(rect.X + rect.Width - cornerRadius * 2, rect.Y, cornerRadius * 2, cornerRadius * 2, 270, 90);
            roundedRect.AddLine(rect.Right, rect.Y + cornerRadius * 2, rect.Right, rect.Y + rect.Height - cornerRadius * 2);
            roundedRect.AddArc(rect.X + rect.Width - cornerRadius * 2, rect.Y + rect.Height - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
            roundedRect.AddLine(rect.Right - cornerRadius * 2, rect.Bottom, rect.X + cornerRadius * 2, rect.Bottom);
            roundedRect.AddArc(rect.X, rect.Bottom - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 90, 90);
            roundedRect.AddLine(rect.X, rect.Bottom - cornerRadius * 2, rect.X, rect.Y + cornerRadius * 2);
            roundedRect.CloseFigure();
            return roundedRect;
        }

        /// <summary>
        /// 重写布局事件
        /// </summary>
        /// <param name="levent"></param>
        protected override void OnLayout(LayoutEventArgs levent)
        {
            base.OnLayout(levent);
            DrawBackgroundImage();
        }
    }
}
