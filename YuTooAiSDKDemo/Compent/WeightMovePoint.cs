﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace YoYo.YuTooAiSDK.Demo.Compent
{
    public partial class WeightMovePoint : UserControl
    {
        /// <summary>
        /// 上一次的有效大小
        /// </summary>
        private int lastSize = 40;


        public WeightMovePoint()
        {
            InitializeComponent();
            // 初始化控件
            Size = new Size(lastSize, lastSize);
            BackColor = Color.Blue;
            CreateEllipse();
        }

        /// <summary>
        /// 创建圆形控件
        /// </summary>
        private void CreateEllipse()
        {
            // 创建路径对象
            GraphicsPath path = new GraphicsPath();
            // 在路径上绘制圆路径
            path.AddEllipse(new RectangleF(0, 0, Width, Height));
            // 创建画布
            Bitmap bm = new Bitmap(Width, Height);
            var g = Graphics.FromImage(bm);
            // 绘制弧线，用于优化锯齿
            g.FillPath(new SolidBrush(BackColor), path);
            g.DrawArc(new Pen(BackColor, 4), new RectangleF(0, 0, Width, Height), 0, 360);
            // 将控件调整为圆形
            Region = new Region(path);
        }

        /// <summary>
        /// 重写布局事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLayout(LayoutEventArgs e)
        {
            base.OnLayout(e);
            // 创建圆形控件
            CreateEllipse();
        }

        /// <summary>
        /// 重写大小变化事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            // 强制为偶数
            if (Width % 2 != 0)
            {
                Width++;
            }
            if (Height % 2 != 0)
            {
                Height++;
            }
            // 强制宽高相等
            if (Width != lastSize)
            {
                Height = Width;
            }
            else if (Height != lastSize)
            {
                Width = Height;
            }
            // 缓存新值
            lastSize = Width;
        }
    }
}
