﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YoYo.YuTooAiSDK.Demo.Compent
{
    public partial class WeightSimulation : UserControl
    {
        /// <summary>
        /// 重量点是否允许被移动
        /// </summary>
        private volatile bool allowMove = false;

        /// <summary>
        /// 滑动距离百分比
        /// </summary>
        [Description("滑动距离百分比")]
        public double Percentage { get; private set; }

        /// <summary>
        /// 重量点移动事件类型
        /// </summary>
        public delegate void WeightMovePointMovedEvent();

        /// <summary>
        /// 重量点移动事件
        /// </summary>
        [Description("重量点移动事件")]
        public event WeightMovePointMovedEvent WeightPointMoved;

        public WeightSimulation()
        {
            InitializeComponent();
            // 绘制背景条的大小
            WeightBackBar.Width = Width;
            WeightBackBar.Height = Height / 3;
            WeightBackBar.Location = new Point()
            {
                X = 0,
                Y = (Height - WeightBackBar.Height) / 2
            };
            // 重新绘制移动点的大小
            WeightMovePoint.Height = Height;
            WeightMovePoint.Width = Height;
        }

        /// <summary>
        /// 重写大小变化事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            // 绘制背景条的大小
            WeightBackBar.Width = Width;
            WeightBackBar.Height = Height / 3;
            WeightBackBar.Location = new Point()
            {
                X = 0,
                Y = (Height - WeightBackBar.Height) / 2
            };
            // 重新绘制移动点的大小
            WeightMovePoint.Height = Height;
            WeightMovePoint.Width = Height;
        }

        /// <summary>
        /// 重写布局事件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLayout(LayoutEventArgs e)
        {
            base.OnLayout(e);
            // 绘制背景条的大小
            WeightBackBar.Width = Width;
            WeightBackBar.Height = Height / 3;
            WeightBackBar.Location = new Point()
            {
                X = 0,
                Y = (Height - WeightBackBar.Height) / 2
            };
            // 重新绘制移动点的大小
            WeightMovePoint.Height = Height;
            WeightMovePoint.Width = Height;
        }

        /// <summary>
        /// 监听重量点被按下事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WeightMovePoint_MouseDown(object sender, MouseEventArgs e)
        {
            allowMove = true;
        }

        /// <summary>
        /// 监听重量点被松开事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WeightMovePoint_MouseUp(object sender, MouseEventArgs e)
        {
            allowMove = false;
        }

        /// <summary>
        /// 监听重量被移动事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WeightMovePoint_MouseMove(object sender, MouseEventArgs e)
        {
            // 是否允许被移动
            if (allowMove)
            {
                // 配置限制范围
                int minX = 0;
                int maxX = Width - WeightMovePoint.Width;
                // 判断移动后的位置
                int x = WeightMovePoint.Left + e.X - (WeightMovePoint.Width / 2);
                if (x < minX)
                {
                    x = minX;
                }
                else if (x > maxX)
                {
                    x = maxX;
                }
                // 移动标记点
                WeightMovePoint.Left = x;
                // 计算X轴占可用距离的百分比
                Percentage = (double)x / maxX * 100;
                // 触发移动事件
                WeightPointMoved?.Invoke();
            }
        }
    }
}
