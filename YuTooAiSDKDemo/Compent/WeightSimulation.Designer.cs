﻿
namespace YoYo.YuTooAiSDK.Demo.Compent
{
    partial class WeightSimulation
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeightSimulation));
            this.WeightMovePoint = new WeightMovePoint();
            this.WeightBackBar = new WeightBackBar();
            this.SuspendLayout();
            // 
            // WeightMovePoint
            // 
            this.WeightMovePoint.BackColor = System.Drawing.Color.Blue;
            this.WeightMovePoint.Location = new System.Drawing.Point(0, 0);
            this.WeightMovePoint.Margin = new System.Windows.Forms.Padding(0);
            this.WeightMovePoint.Name = "WeightMovePoint";
            this.WeightMovePoint.Size = new System.Drawing.Size(60, 60);
            this.WeightMovePoint.TabIndex = 1;
            this.WeightMovePoint.MouseDown += new System.Windows.Forms.MouseEventHandler(this.WeightMovePoint_MouseDown);
            this.WeightMovePoint.MouseMove += new System.Windows.Forms.MouseEventHandler(this.WeightMovePoint_MouseMove);
            this.WeightMovePoint.MouseUp += new System.Windows.Forms.MouseEventHandler(this.WeightMovePoint_MouseUp);
            // 
            // WeightBackBar
            // 
            this.WeightBackBar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.WeightBackBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("WeightBackBar.BackgroundImage")));
            this.WeightBackBar.Location = new System.Drawing.Point(0, 20);
            this.WeightBackBar.Margin = new System.Windows.Forms.Padding(0);
            this.WeightBackBar.Name = "WeightBackBar";
            this.WeightBackBar.Size = new System.Drawing.Size(300, 20);
            this.WeightBackBar.TabIndex = 0;
            // 
            // WeightSimulation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.WeightMovePoint);
            this.Controls.Add(this.WeightBackBar);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "WeightSimulation";
            this.Size = new System.Drawing.Size(300, 60);
            this.ResumeLayout(false);

        }

        #endregion

        private WeightBackBar WeightBackBar;
        private WeightMovePoint WeightMovePoint;
    }
}
