﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using YoYo.YuTooAiPb;
using System.Drawing;
#if NET45
using YoYo.YuTooAiSDK
#endif

namespace YoYo.YuTooAiSDK.Demo
{
    public partial class YuTooAiSDKDemo : Form
    {
        /// <summary>
        /// 日志记录器实例
        /// </summary>
        private AiLogger log = new AiLogger();

        /// <summary>
        /// SDK操作对象
        /// </summary>
        private AiGrpc sdk = new AiGrpc();

        /// <summary>
        /// 异步线程刷新托管类型
        /// </summary>
        delegate void FlushEvent();

        /// <summary>
        /// 调用控件事件的托管类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        delegate void TriggerControlEvent(object sender, EventArgs e);

        /// <summary>
        /// 日志缓存队列
        /// </summary>
        private readonly ConcurrentQueue<string> logQueue = new ConcurrentQueue<string>();

        /// <summary>
        /// 全局重量信息(克)
        /// </summary>
        private volatile int globalWeight = 0;

        /// <summary>
        /// AI识别（流式）参数队列
        /// </summary>
        private BlockingCollection<AiMatchingRequest> aiMatchingReqQueue;

        /// <summary>
        /// 缓存的最后一次的识别ID
        /// </summary>
        private long identifyID;

        /// <summary>
        /// 构造方法
        /// </summary>
        public YuTooAiSDKDemo()
        {
            InitializeComponent();
            // 赋值日志记录器实例
            log.SetPrint(LogPrint);
            // 实例化SDK类
            sdk = new AiGrpc(LogPrint);
        }

        /// <summary>
        /// 刷新日志内容
        /// </summary>
        private void FlushLogBox()
        {
            while(logQueue.Count > 0)
            {
                if(logQueue.TryDequeue(out string str))
                {
                    LogPrintBox.AppendText(str);
                    LogPrintBox.AppendText("\r\n");
                }
            }
        }

        /// <summary>
        /// 日志记录器（异步线程访问）
        /// </summary>
        /// <param name="level">日志级别</param>
        /// <param name="msg">日志内容</param>
        public void LogPrint(AiLogger.Level level, string fileName, string methodName, int lineNum, string msg)
        {
            try
            {
                // 判断数据是否大于1000条日志
                if (logQueue.Count >= 1000)
                {
                    // 移除第一条
                    logQueue.TryDequeue(out string _);
                }

                // 提取简单文件名
                fileName = System.IO.Path.GetFileName(fileName);
                // 追加日志
                logQueue.Enqueue($"{DateTime.Now} [{AiLogger.LevelToChar(level)}] [{fileName}:{lineNum}] {msg}");
                // 异步线程不允许直接访问组件
                LogPrintBox.Invoke((FlushEvent)FlushLogBox);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// 获取商品数据（数据来源可自行修改）
        /// </summary>
        /// <returns></returns>
        public PluSyncRequest GetPluData()
        {
            // 初始化参数
            var res = new PluSyncRequest
            {
                // 标记商品为非演示数据
                IsDemo = false
            };

            // 模拟数据源
            List<PluStatusInfo> pluData = new List<PluStatusInfo> {
                new PluStatusInfo
                {
                    Plu = new PluInfo
                    {
                        Code="1000",
                        Name="香蕉"
                    },
                    IsLock=false,
                    IsSale=true,
                },
                new PluStatusInfo
                {
                    Plu = new PluInfo
                    {
                        Code="1001",
                        Name="苹果"
                    },
                    IsLock=false,
                    IsSale=true
                },
                new PluStatusInfo
                {
                    Plu = new PluInfo
                    {
                        Code="1002",
                        Name="西瓜"
                    },
                    IsLock=false,
                    IsSale=true
                },
                new PluStatusInfo
                {
                    Plu = new PluInfo
                    {
                        Code="1003",
                        Name="白菜"
                    },
                    IsLock=false,
                    IsSale=true
                },
                new PluStatusInfo
                {
                    Plu = new PluInfo
                    {
                        Code="1004",
                        Name="水蜜桃"
                    },
                    IsLock=false,
                    IsSale=true
                },
                new PluStatusInfo
                {
                    Plu = new PluInfo
                    {
                        Code="1005",
                        Name="茄子"
                    },
                    IsLock=false,
                    IsSale=true
                },
                new PluStatusInfo
                {
                    Plu = new PluInfo
                    {
                        Code="2305",
                        Name="草莓"
                    },
                    IsLock=false,
                    IsSale=true
                },
            };

            // 追加数据
            res.PluList.AddRange(pluData);

            // 返回商品数据
            return res;
        }

        /// <summary>
        /// 刷新UI上的Grpc端口号
        /// </summary>
        public void FlushGrpcPort()
        {
            // 获取当前端口
            int port = sdk.GetAiProcessPort();
            if (port > 0)
            {
                GrpcPortLabel.Text = $"端口: {port}";
            }
            else
            {
                GrpcPortLabel.Text = "端口: null";
            }
        }

        /// <summary>
        /// 封装SDK启动逻辑
        /// </summary>
        private void SdkRun(string vendorCode, string vendorKey)
        {
            // 异步处理
            Task.Run(() =>
            {
                try
                {
                    // 判断入参
                    if (string.IsNullOrEmpty(vendorCode))
                    {
                        log.Error("厂商标识不能为空");
                        return;
                    }
                    else if (string.IsNullOrEmpty(vendorKey) || vendorKey.Length != 32)
                    {
                        log.Error("厂商密钥不能为空");
                        return;
                    }

                    // 启动AI进程(建议将vendorCode, vendorKey写死传入到Run中，不建议通过控件传入)
                    log.Info("AI识别进程启动中···");
                    var reply = sdk.Run(vendorCode, vendorKey, GetPluData, "./sdk");
                    if (reply == null)
                    {
                        log.Error("Run返回空结果");
                        return;
                    }
                    if (reply.Code != ReplyCodeType.Success)
                    {
                        log.Error(reply.Msg);
                        return;
                    }
                    log.Info("AI识别进程启动成功");

                    //初始化AI服务
                    this.Invoke((TriggerControlEvent)BtnInit_Click, null, null);

                    // 获取设备信息
                    this.Invoke((TriggerControlEvent)BtnGetDeviceInfo_Click, null, null);

                    // 同步商品数据
                    this.Invoke((TriggerControlEvent)BtnPluSync_Click, null, null);
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }
            });
        }

        /// <summary>
        /// 主窗口加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void YuTooAiSDKDemo_Load(object sender, EventArgs e)
        {
            // 异步刷新UI上的端口号
            Task.Run(() =>
            {
                try
                {
                    // 每3秒调用一次
                    while (GrpcPortLabel != null)
                    {
                        // 委托调用
                        GrpcPortLabel?.Invoke((FlushEvent)FlushGrpcPort);
                        Thread.Sleep(3000);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }
            });

            // (建议将vendorCode, vendorKey写死传入到Run中，不建议通过控件传入)
            // 获取vendorCode和vendorKey
            string vendorCode = VendorCodeInput.Text.Trim();
            string vendorKey = VendorKeyInput.Text.Trim();

            // 启动SDK
            SdkRun(vendorCode, vendorKey);
        }

        /// <summary>
        /// 窗体关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void YuTooAiSDKDemo_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                // 执行服务释放
                BtnUnInit_Click(null, null);
                // 彻底退出
                Application.Exit();
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 重量变化事件
        /// </summary>
        private void WeightSimulation_WeightPointMoved()
        {
            if (WeightSimulation.Percentage < 10)
            {
                WeightSimulationBox.Text = "重量：欠载";
                WeightSimulationBox.ForeColor = Color.Orange;
                // 更新全局重量
                globalWeight = 0;
            }
            else if (WeightSimulation.Percentage > 90)
            {
                WeightSimulationBox.Text = "重量：超载";
                WeightSimulationBox.ForeColor = Color.Red;
                // 更新全局重量
                globalWeight = 0;
            }
            else
            {
                // 更新全局重量
                globalWeight = (int)((WeightSimulation.Percentage - 10) * 30);
                WeightSimulationBox.Text = $"重量：{globalWeight}g";
                WeightSimulationBox.ForeColor = Color.Green;
            }
        }

        /// <summary>
        /// 点击服务启动按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAiServerRun_Click(object sender, EventArgs e)
        {
            try
            {
                // (建议将vendorCode, vendorKey写死传入到Run中，不建议通过控件传入)
                // 获取vendorCode和vendorKey
                string vendorCode = VendorCodeInput.Text.Trim();
                string vendorKey = VendorKeyInput.Text.Trim();

                // 启动SDK
                SdkRun(vendorCode, vendorKey);
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 点击服务初始化按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnInit_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("AI服务初始化中···");
                var reply = sdk.Init();
                if (reply == null)
                {
                    log.Error("Init返回空结果");
                    return;
                }
                if (reply.Code != ReplyCodeType.Success)
                {
                    log.Error(reply.Msg);
                    return;
                }
                log.Info("AI服务初始化成功");
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 点击服务释放按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnUnInit_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("识别服务释放中···");
                // 执行服务释放
                var reply = sdk.UnInit();
                if (reply == null)
                {
                    log.Error("UnInit返回空结果");
                }
                else if (reply.Code != ReplyCodeType.Success)
                {
                    log.Error(reply.Msg);
                }
                else
                {
                    log.Info("识别服务释放成功");
                }
                // 释放AI识别（流式）参数队列资源
                aiMatchingReqQueue?.Dispose();
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
            finally
            {
                // 释放AI识别（流式）参数队列
                aiMatchingReqQueue = null;
            }
        }

        /// <summary>
        /// 点击设备激活按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDeviceActivate_Click(object sender, EventArgs e)
        {
            try
            {
                // 执行激活
                log.Info("设备激活中···");
                var reply = sdk.Activate(new ActivateRequest
                {
                    ActivateInfo = new ActivateInfo
                    {
                        CdKey = CdKeyInput.Text, // 激活码
                        ShopInfo = new ShopBaseInfo // 门店基础信息
                        {
                            Name = ShopNameInput.Text,// 门店名称
                            Contact = ContactInput.Text,// 联系人
                            Phone = PhoneInput.Text // 联系电话
                        }
                    }
                });
                if (reply == null)
                {
                    log.Error("Activate返回空结果");
                    return;
                }
                if (reply.Code != ReplyCodeType.Success)
                {
                    log.Error(reply.Msg);
                    return;
                }
                log.Info("设备激活成功");
                // 激活成功，同步商品数据
                BtnPluSync_Click(sender, e);
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 点击调整识别区域
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnShowCameraAreaView_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("识别区域配置窗口打开中···");
                // 渲染识别区域配置窗口
                var reply = sdk.ShowCameraAreaView();
                if (reply == null)
                {
                    log.Error("ShowCameraAreaView返回空结果");
                    return;
                }
                if (reply.Code != ReplyCodeType.Success)
                {
                    log.Error(reply.Msg);
                    return;
                }
                log.Info("识别区域配置窗口打开成功");
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 获取设备信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGetDeviceInfo_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("设备信息获取中···");
                // 获取设备信息
                var reply = sdk.GetDeviceInfo();
                if (reply == null || reply.Status == null)
                {
                    log.Error("GetDeviceInfo返回空结果");
                }
                if (reply.Status.Code != ReplyCodeType.Success)
                {
                    log.Error(reply.Status.Msg);
                    return;
                }
                // 打印一下内容
                log.Debug(reply.ToString());
                // 赋值控件信息
                CdKeyInput.Text = reply.ActivateInfo.CdKey;
                ShopNameInput.Text = reply.ActivateInfo.ShopInfo.Name;
                ContactInput.Text = reply.ActivateInfo.ShopInfo.Contact;
                PhoneInput.Text = reply.ActivateInfo.ShopInfo.Phone;
                log.Info("设备信息获取成功");
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 点击同步商品数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPluSync_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("商品数据同步中···");
                // 记录一下商品数据
                var pluData = GetPluData();
                log.Debug(pluData.ToString());
                // 同步商品信息
                var reply = sdk.PluSync(pluData);
                if (reply == null)
                {
                    log.Error("PluSync返回空结果");
                    return;
                }
                if (reply.Code != ReplyCodeType.Success)
                {
                    log.Error(reply.Msg);
                    return;
                }
                log.Info("商品数据同步成功");
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 点击AI识别按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAiMatching_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("-------------------- AI识别开始 --------------------");
                // 构建识别参数
                AiMatchingRequest aiMatchingRequest = new AiMatchingRequest
                {
                    MaxNum = 5,// 限制输出结果数量
                    MatchType = MatchingType.Forcibly,
                    Weight = globalWeight,
                    Stable = WeightStatusType.Stable
                };
                log.Info($"识别参数：{aiMatchingRequest}");
                // 执行一次识别
                var reply = sdk.AiMatching(aiMatchingRequest);
                if (reply == null || reply.Status == null)
                {
                    log.Error("AiMatching返回空结果");
                    return;
                }
                if (reply.Status.Code != ReplyCodeType.Success)
                {
                    log.Error($"识别异常：{reply.Status.Msg}");
                    return;
                }
                log.Info($"识别成功：{reply.Status.Msg}");
                log.Info($"识别请求入参：{reply.AiMatchingRequest}");
                log.Info($"识别ID：{reply.IdentifyID}");
                log.Info($"重试机制：第{reply.MatchIndex}次");
                log.Info($"识别结果：{reply.PluList.Count}个");
                int index = 0;
                foreach (var item in reply.PluList)
                {
                    index++;
                    log.Info($"{index}、 {item.Code} | {item.Name}");
                }
                // 缓存识别ID
                identifyID = reply.IdentifyID;
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
            finally
            {
                log.Info("-------------------- AI识别结束 --------------------");
            }
        }

        /// <summary>
        /// 点击AI识别（流式）按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAiMatchingStream_Click(object sender, EventArgs e)
        {
            try
            {
                // 初始化识别参数队列
                aiMatchingReqQueue = new BlockingCollection<AiMatchingRequest>();
                // 使用异步线程流式识别
                PublicReply res = sdk.AiMatchingStream(aiMatchingReqQueue, (reply) =>
                {
                    try
                    {
                        log.Info("---------- 》》》回调了一个识别结果》》》 ----------");
                        if (reply == null || reply.Status == null)
                        {
                            log.Error("AiMatchingStream回调了空结果");
                            return true;
                        }
                        if (reply.Status.Code != ReplyCodeType.Success)
                        {
                            log.Error($"识别异常：{reply.Status.Msg}");
                            return true;
                        }
                        log.Info($"识别成功：{reply.Status.Msg}");
                        log.Info($"识别请求入参：{reply.AiMatchingRequest}");
                        log.Info($"识别ID：{reply.IdentifyID}");
                        log.Info($"重试机制：第{reply.MatchIndex}次");
                        log.Info($"识别结果：{reply.PluList.Count}个");
                        int index = 0;
                        foreach (var item in reply.PluList)
                        {
                            index++;
                            log.Info($"{index}、 {item.Code} | {item.Name}");
                        }
                        // 缓存识别ID
                        identifyID = reply.IdentifyID;
                    }
                    catch (Exception ex)
                    {
                        log.Fatal(ex.Message);
                    }
                    finally
                    {
                        log.Info("---------- 《《《回调了一个识别结果《《《 ----------");
                    }
                    return true;
                });

                if (res == null)
                {
                    log.Error("AiMatchingStream返回空结果");
                    return;
                }
                if (res.Code != ReplyCodeType.Success)
                {
                    log.Error(res.Msg);
                    return;
                }

                // 异步每100ms获取一次重量
                Task.Run(() =>
                {
                    // 模拟取重
                    try
                    {
                        log.Info("-------------------- AI识别(流式)开始 --------------------");

                        // 重量相同计数器
                        int weightEqCount = 0;
                        // 上一次的重量
                        int lastWeight = globalWeight;
                        // 队列不为空时开始循环
                        while (aiMatchingReqQueue != null)
                        {
                            // 缓存本次重量
                            int cacheWeight = globalWeight;
                            // 判断本次与上次释放相同
                            if (lastWeight == cacheWeight)
                            {
                                weightEqCount++;
                            }
                            else
                            {
                                weightEqCount = 0;
                            }

                            // 将本次重量缓存
                            lastWeight = cacheWeight;
                            // 判断稳定状态
                            WeightStatusType stabel = WeightStatusType.Unstable;
                            if (cacheWeight < 0)
                            {
                                // 欠载
                                stabel = WeightStatusType.Underload;
                            }
                            else if (cacheWeight > 1000)
                            {
                                // 超载
                                stabel = WeightStatusType.Overload;
                            }
                            else if (weightEqCount >= 3)
                            {
                                // 稳定
                                stabel = WeightStatusType.Stable;
                            }

                            // 构建识别参数,并放入队列
                            aiMatchingReqQueue?.Add(new AiMatchingRequest
                            {
                                MaxNum = 5,// 限制输出结果数量
                                MatchType = MatchingType.Auto,
                                Weight = cacheWeight,
                                Stable = stabel
                            });

                            // 等待100ms
                            Thread.Sleep(100);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }
                    finally
                    {
                        log.Info("-------------------- AI识别(流式)结束 --------------------");
                    }
                });
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 点击AI标记按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAiMark_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info("-------------------- AI标记开始 --------------------");
                // 构建标记参数
                AiMarkRequest aiMarkRequest = new AiMarkRequest
                {
                    IdentifyID = identifyID, // 提取缓存的识别ID
                    MarkType = MarkType.Search,
                    Plu = new PluInfo
                    {
                        Code = "1001",
                    },
                    PluSaleInfo = new PluSaleInfo
                    {
                        Sales = 20,
                        PriceUnit = PriceUnitType.Weight,
                        Price = 100,
                        SalePrice = 80,
                        Amount = 80 * 20,
                        BarCode = "LS9853P1001"
                    }
                };
                log.Info($"标记参数：{aiMarkRequest}");
                // 执行一次识别
                var reply = sdk.AiMark(aiMarkRequest);
                if (reply == null)
                {
                    log.Error("AiMark返回空结果");
                    return;
                }
                if (reply.Code != ReplyCodeType.Success)
                {
                    log.Error($"{reply.Msg}");
                    return;
                }
                log.Info($"{reply.Msg}");
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
            finally
            {
                log.Info("-------------------- AI标记结束 --------------------");
            }
        }

        // 数据来源标识
        private string onlyTag = "192.168.3.147";
        // 上一次导出学习数据的时间戳
        private long cacheTmpImportTimestamp = 0;

        /// <summary>
        /// 点击获取学习数据按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGetStudyData_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info($"学习数据获取中···");
                // 执行学习数据获取
                GetStudyDataReply reply = sdk.GetStudyData(new GetStudyDataRequest()
                {
                    Mode = GetStudyDataModeType.AllMode,
                    LastTimestamp = 0 // 学习数据导入时间
                });
                if (reply == null || reply.Status == null)
                {
                    log.Error("GetStudyData响应结果为空");
                    return;
                }
                if (reply.Status.Code != ReplyCodeType.Success)
                {
                    log.Error($"GetStudyData调用失败, {reply.Status.Msg}");
                    return;
                }
                // 判断学习数据是否为空
                if (reply.StudyData.IsEmpty)
                {
                    log.Warn("获取到的学习数据为空");
                    return;
                }
                // 选择文件夹
                FolderBrowserDialog dialog = new FolderBrowserDialog
                {
                    Description = "请选择保存路径"
                };
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    log.Warn("无操作");
                    return;
                }
                if (string.IsNullOrEmpty(dialog.SelectedPath))
                {
                    log.Warn("未选择保存路径");
                    return;
                }
                // 构建文件名
                string fileName = $"{dialog.SelectedPath}\\studydata.zip";
                if (!string.IsNullOrEmpty(reply.StudyDataMd5))
                {
                    fileName = $"{dialog.SelectedPath}\\{reply.StudyDataMd5}.zip";
                }
                // 学习数据保存起来
                using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    // 写入到文件
                    fs.Write(reply.StudyData.ToByteArray(), 0, reply.StudyData.Length);
                    // 刷新到文件中
                    fs.Flush();
                }
                log.Info($"学习数据获取成功, {fileName}");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        /// <summary>
        /// 点击配置学习数据按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSetStudyData_Click(object sender, EventArgs e)
        {
            try
            {
                log.Info($"学习数据配置中···");
                OpenFileDialog openFileDialog = new OpenFileDialog
                {
                    Filter = "*.zip|",
                    RestoreDirectory = true,
                    FilterIndex = 1
                };
                if (openFileDialog.ShowDialog() != DialogResult.OK)
                {
                    log.Error("无操作");
                    return;
                }
                // 查询文件名和MD5
                string fileName = openFileDialog.FileName;
                string fileMd5 = Path.GetFileNameWithoutExtension(fileName); // 正式环境请从数据库或其他持久化储存中获取MD5
                if (string.IsNullOrEmpty(fileName))
                {
                    log.Warn("学习文件名不能为空");
                    return;
                }
                if (string.IsNullOrEmpty(fileMd5) || fileMd5.Length != 32)
                {
                    log.Warn("学习数据MD5不合法");
                    return;
                }
                try
                {
                    // 构建学习数据参数
                    SetStudyDataRequest setStudyDataRequest = new SetStudyDataRequest
                    {
                        Mode = SetStudyDataModeType.Merger, // 以合并方式融合数据
                        StudyDataMd5 = fileMd5,
                    };
                    // 获取文件中的学习数据
                    using (FileStream fs = new FileStream(fileName, FileMode.Open))
                    {
                        // 声明一个字节数组，用于一次性读取所有学习数据
                        byte[] data = new byte[fs.Length];
                        int count = fs.Read(data, 0, (int)fs.Length);
                        if (count != (int)fs.Length)
                        {
                            log.Error("读取学习数据失败");
                            return;
                        }
                        // 追加到参数中
                        setStudyDataRequest.StudyData = Google.Protobuf.ByteString.CopyFrom(data);
                    };
                    // 执行学习数据同步
                    PublicReply reply = sdk.SetStudyData(setStudyDataRequest);
                    if (reply.Code != ReplyCodeType.Success)
                    {
                        log.Error(reply.Msg);
                        return;
                    }
                    log.Info($"学习数据配置成功");
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        private void BtnStress_Click(object sender, EventArgs e)
        {
            var stressForm = new StressForm(ref sdk, ref log);
            stressForm.ShowDialog();
        }
    }
}
