﻿using Grpc.Core;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
#if NET5_0_OR_GREATER // .NET5.0及以上版本支持跨平台，需要判断平台
using System.Runtime.InteropServices;
#endif // .NET5.0及以上版本支持跨平台，需要判断平台
using System.Threading;
using System.Threading.Tasks;
using YoYo.YuTooAiPb;

namespace YoYo.YuTooAiSDK
{
    internal class AiProcess
    {
        /// <summary>
        /// 日志记录器实例
        /// </summary>
        private readonly AiLogger log = new AiLogger();

        /// <summary>
        /// AI进程的工作目录
        /// </summary>
        private string workDir = ".";

        /// <summary>
        /// AI识别进程名称
        /// </summary>
        private const string aiProcessName = "yutooaisdk.exe";

        /// <summary>
        /// AI进程使用的端口号
        /// </summary>
        public int AiProcessPort { get; private set; }

        /// <summary>
        /// 程序是否发出了退出通知
        /// </summary>
        private volatile bool exited = true;

        /// <summary>
        /// gRPC通信管道连接状态
        /// </summary>
        private volatile ChannelState channelState = ChannelState.Idle;

        /// <summary>
        /// 由图智能识别SDK公开服务客户端
        /// </summary>
        public YoYoAiPublic.YoYoAiPublicClient PublicClient { get; private set; }

        /// <summary>
        /// 由图智能识别SDK私有服务客户端
        /// </summary>
        public YoYoAiPrivate.YoYoAiPrivateClient PrivateClient { get; private set; }

        /// <summary>
        /// AI进程事件托管方法
        /// </summary>
        public delegate void AiProcessEventFunc();

        /// <summary>
        /// gRPC连接成功事件
        /// </summary>
        public AiProcessEventFunc GrpcConnectedEvent { get; set; }

        /// <summary>
        /// AI进程退出事件
        /// </summary>
        public AiProcessEventFunc ExitEvent { get; set; }

        /// <summary>
        /// 系统关机事件
        /// </summary>
        public AiProcessEventFunc SystemShutdownEvent { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logPrint"></param>
        public AiProcess(AiLogger.Print logPrint = null)
        {
            // 赋值日志记录器
            log.SetPrint(logPrint);
        }

        /// <summary>
        /// 杀掉上一个运行中的AI识别进程
        /// </summary>
        private void KillAiProcess()
        {
            try
            {
                // 需要Kill上一个运行的程序
                string findName = aiProcessName;
                int index = findName.LastIndexOf(".exe");
                if (index > 0)
                {
                    findName = findName.Remove(index);
                }
                Process[] workers = Process.GetProcessesByName(findName);
                foreach (Process worker in workers)
                {
                    try
                    {
                        worker.Kill();
                        worker.WaitForExit();
                        worker.Dispose();
                    }
                    catch (Exception e)
                    {
                        log.Warn(e.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        /// <summary>
        /// 重置gRPC客户端
        /// </summary>
        private void ReSetGrpcClient()
        {
            // 重置客户端
            channelState = ChannelState.Idle;
            PublicClient = null;
            PrivateClient = null;
            AiProcessPort = 0;
        }

        /// <summary>
        /// 连接到gRPC服务器
        /// </summary>
        /// <param name="port">gRPC端口号</param>
        private bool ConnectGrpcChannel(int port)
        {
            try
            {
                // 设置数据传输大小
                List<ChannelOption> channelOptions = new List<ChannelOption>
                {
                    new ChannelOption(ChannelOptions.MaxReceiveMessageLength, 100 * 1024 * 1024),
                    new ChannelOption(ChannelOptions.MaxSendMessageLength, 100 * 1024 * 1024)
                };
                // 使用无证书方式创建通信管道
                Channel channel = new Channel($"127.0.0.1:{port}", ChannelCredentials.Insecure, channelOptions);
                // 等待管道连接完成，最多等待30秒
                channel.ConnectAsync();
                for (int i = 0; i < 30 * 1000; i += 500)
                {
                    // 赋值通信管道状态
                    channelState = channel.State;
                    // 判断状态是否为Ready
                    if (channelState == ChannelState.Ready)
                    {
                        // 连接成功，跳出
                        break;
                    }
                    // 睡眠500毫秒
                    Thread.Sleep(500);
                }

                // 判断状态是否为Ready
                if (channelState != ChannelState.Ready)
                {
                    // 连接失败了
                    log.Fatal($"gRPC channel connect failed, channel state [{channel.State}]");
                    return false;
                }


                // 创建客户端
                PublicClient = new YoYoAiPublic.YoYoAiPublicClient(channel);
                PrivateClient = new YoYoAiPrivate.YoYoAiPrivateClient(channel);

                // 执行gRPC连接成功事件
                GrpcConnectedEvent?.Invoke();

                // 连接成功
                return true;
            }
            catch (Exception e)
            {
                // 连接失败了
                log.Fatal($"gRPC channel connect failed, exception message [{e}]");
                return false;
            }
        }

        /// <summary>
        /// 启动AI识别进程
        /// </summary>
        /// <param name="port">gRPC端口号</param>
        /// <param name="vendorCode">厂商标识</param>
        /// <param name="vendorKey">厂商密钥</param>
        private void StartAiProcess(int port, string vendorCode, string vendorKey)
        {
            try
            {
                // 进程已接收到结束时就不要再启动了
                if (exited)
                {
                    return;
                }

                // 记录参数
                string args = $"-p {port} -c {vendorCode} -k {vendorKey}";
                log.Info($"yutooaisdk process start arguments【{args}】···");

                // 创建一个进程启动信息实例
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    // 可执行文件路径
                    FileName = $"{workDir}\\{aiProcessName}",
                    // 启动参数
                    Arguments = args,
                    // 是否需要启动shell窗口
                    UseShellExecute = false,
                    // 是否在无窗口启动该进程
                    CreateNoWindow = true,
                    // 是否重定向输入
                    //RedirectStandardInput = false,
                    // 是否重定向输出
                    //RedirectStandardOutput = false,
                    // 是否重定向错误
                    RedirectStandardError = true,
                    // 输出内容编码
                    //StandardOutputEncoding = System.Text.Encoding.UTF8,
                    // 错误内容编码
                    StandardErrorEncoding = System.Text.Encoding.UTF8,
                    // 指定进程的工作路径
                    WorkingDirectory = workDir
                };

                // 创建一个进程实例
                Process process = new Process
                {
                    StartInfo = startInfo,
                };

                // 启动进程
                bool ok = process.Start();
                if (!ok)
                {
                    // 进程启动失败，重新启动进程
                    log.Fatal("yutooaisdk process start failed, process.Start() == false");
                    return;
                }

                //// 接收重定向的输出信息
                //process.OutputDataReceived += (s, e) =>
                //{
                //    log.Fatal($"yutooaisdk process standard output message: {e.Data}");
                //};

                // 接收重定向的错误信息
                process.ErrorDataReceived += (s, e) =>
                {
                    if (!exited)
                    {
                        log.Fatal($"yutooaisdk process standard error message: {e.Data}");
                    }
                };

                // 通知重定向错误信息开始
                //process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                // 创建客户端连接
                ok = ConnectGrpcChannel(port);
                if (!ok)
                {
                    // 连接失败了，关闭进程，重新启动进程
                    process.Close();
                    return;
                }

                // AI进程运行成功
                log.Info("yutooaisdk process running···");

                // 等待进程结束，重新启动进程
                process.WaitForExit();
                log.Fatal($"yutooaisdk process exited, exit code: {process.ExitCode}");
            }
            catch (Exception e)
            {
                // 记录到日志中，重新启动进程
                log.Fatal($"yutooaisdk process exited, exception message: {e}");
            }
        }

        /// <summary>
        /// AI进程的守护线程
        /// </summary>
        /// <param name="vendorCode">厂商标识</param>
        /// <param name="vendorKey">厂商密钥</param>
        private void ListenAiProcess(string vendorCode, string vendorKey)
        {
            // AI进程守护线程
            Task.Run(() =>
            {
                // 是否为首次启动
                bool onceStart = true;
                // 程序未接收到退出通知时将会保持AI进程的活跃状态
                while (!exited)
                {
                    try
                    {
                        // 根据计数器记录日志
                        if (onceStart)
                        {
                            log.Info("yutooaisdk process start···");
                            // 标记为非首次启动
                            onceStart = false;
                        }
                        else
                        {
                            log.Info("yutooaisdk process restart···");
                        }

                        // 需要Kill上一个运行的程序
                        KillAiProcess();
                        // 生成随机端口
                        int port = AiTools.GetRandomPort();
                        // 赋值AI进程使用的端口号
                        AiProcessPort = port;
                        // 启动进程
                        StartAiProcess(port, vendorCode, vendorKey);
                        // 重置gRPC客户端
                        ReSetGrpcClient();
                        // 执行进程退出事件
                        ExitEvent?.Invoke();
                        // 持续保活AI进程,100毫秒后重启进程
                        Thread.Sleep(100);
                    }
                    catch (Exception ex)
                    {
                        log.Fatal(ex.Message);
                    }
                }
            });
        }

        /// <summary>
        /// 操作系统关机或注销事件
        /// </summary>
        /// <param name="sender">发送信息</param>
        /// <param name="e">事件信息</param>
        private void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {

            try
            {
#if NET5_0_OR_GREATER // .NET5.0及以上版本支持跨平台，需要判断平台
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
#endif // .NET5.0及以上版本支持跨平台，需要判断平台
                // 停止守护线程的监听
                exited = true;
                // 通知外部发生了系统关机事件
                SystemShutdownEvent?.Invoke();
                // 允许关闭计算机
                e.Cancel = false;
#if NET5_0_OR_GREATER // .NET5.0及以上版本支持跨平台，需要判断平台
                }
#endif // .NET5.0及以上版本支持跨平台，需要判断平台
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
            }
        }

        /// <summary>
        /// 运行AI识别进程
        /// </summary>
        /// <param name="vendorCode">厂商标识</param>
        /// <param name="vendorKey">厂商密钥</param>
        /// <param name="workPath">SDK工作目录</param>
        public bool Run(string vendorCode, string vendorKey, string workPath = ".")
        {
            try
            {
                // AI进程已退出时需要启动进程
                if (exited)
                {
                    // 赋值工作目录
                    workDir = workPath;
                    // 标识进程已开始
                    exited = false;
                    // 开始异步监听进程
                    ListenAiProcess(vendorCode, vendorKey);
#if NET5_0_OR_GREATER // .NET5.0及以上版本支持跨平台，需要判断平台
                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    {
#endif // .NET5.0及以上版本支持跨平台，需要判断平台
                    // 监听系统关机事件
                    SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);
#if NET5_0_OR_GREATER // .NET5.0及以上版本支持跨平台，需要判断平台
                    }
#endif // .NET5.0及以上版本支持跨平台，需要判断平台

                    //---------- 等待连接成功 ----------//
                    // 间隔时间（毫秒）
                    int intervals = 10;
                    // 持续等待，直至Ready
                    while (channelState != ChannelState.Ready)
                    {
                        // 等待指定间隔时间，继续判断
                        Thread.Sleep(intervals);
                    }
                    return true;
                }
                else
                {
                    log.Error("禁止服务重复启动");
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// AI进程退出守护线程
        /// </summary>
        public void Exit()
        {
            // 标记不需要重启进程
            exited = true;
            // 重置客户端
            ReSetGrpcClient();
            // 需要Kill上一个运行的程序
            KillAiProcess();
        }
    }
}
