﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace YoYo.YuTooAiSDK
{
    /// <summary>
    /// 由图智能识别SDK日志记录器类型声明
    /// </summary>
    public class AiLogger
    {
        /// <summary>
        /// 日志级别枚举
        /// </summary>
        public enum Level
        {
            /// <summary>
            /// 通知级别
            /// </summary>
            TraceLevel = 1,

            /// <summary>
            /// 调试级别
            /// </summary>
            DebugLevel = 2,

            /// <summary>
            /// 普通级别
            /// </summary>
            InfoLevel = 3,

            /// <summary>
            /// 警告级别
            /// </summary>
            WarnLevel = 4,

            /// <summary>
            /// 错误级别
            /// </summary>
            ErrorLevel = 5,

            /// <summary>
            /// 致命级别
            /// </summary>
            FatalLevel = 6
        }

        /// <summary>
        /// 日志级别字符映射
        /// </summary>
        private static readonly Dictionary<Level, string> levelMap = new Dictionary<Level, string>() {
            { Level.TraceLevel , "T" },
            { Level.DebugLevel , "D" },
            { Level.InfoLevel , "I" },
            { Level.WarnLevel , "W" },
            { Level.ErrorLevel , "E" },
            { Level.FatalLevel , "F" },
        };


        /// <summary>
        /// 日志记录器委托方法
        /// </summary>
        /// <param name="level">日志级别</param>
        /// <param name="msg">日志内容</param>
        /// <param name="filename">文件路径</param>
        /// <param name="methodName">方法名称</param>
        /// <param name="lineNum">文件行号</param>
        public delegate void Print(Level level, string filename, string methodName, int lineNum, string msg);

        /// <summary>
        /// 日志记录方法
        /// </summary>
        private Print printFunc;

        /// <summary>
        /// 转换日志级别字符
        /// </summary>
        /// <param name="l">日志级别</param>
        /// <returns></returns>
        public static string LevelToChar(Level l)
        {
            if (levelMap.ContainsKey(l))
            {
                return levelMap[l];
            }
            return "";
        }

        /// <summary>
        /// 赋值日志记录方法
        /// </summary>
        /// <param name="pf"></param>
        public void SetPrint(Print pf)
        {
            printFunc = pf;
        }

        /// <summary>
        /// 日志记录器
        /// </summary>
        /// <param name="level">日志级别</param>
        /// <param name="msg">日志内容</param>
        /// <param name="methodName">调用者方法名称</param>
        /// <param name="fileName">调用者文件名</param>
        /// <param name="lineNum">调用者行数</param>
        private void LogPrint(Level level, string msg, string fileName, string methodName, int lineNum)
        {
            try
            {
                // 异步调用，防止UI或主线程阻塞
                Task.Run(() =>
                {
                    try
                    {
                        // 转义日志中的\0
                        msg = msg.Replace("\0", "\\0");
                        // 日志记录方法不为空时进行调用
                        printFunc?.Invoke(level, fileName, methodName, lineNum, msg);
                    }
                    catch (Exception ex)
                    {
                        // 输出到DebugView
                        Debugger.Log(6, null, ex.Message);
                    }
                }).Wait(50); // 在50毫秒内完成可以最大化保证日志顺序
            }
            catch (Exception ex)
            {
                // 输出到DebugView
                Debugger.Log(6, null, ex.Message);
            }
        }

        /// <summary>
        /// 记录通知级别的日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        /// <param name="methodName">无需传入</param>
        /// <param name="fileName">无需传入</param>
        /// <param name="lineNum">无需传入</param>
        public void Trace(string msg, [CallerMemberName] string methodName = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNum = 0)
        {
            LogPrint(Level.TraceLevel, msg, fileName, methodName, lineNum);
        }

        /// <summary>
        /// 记录调试级别的日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        /// <param name="methodName">无需传入</param>
        /// <param name="fileName">无需传入</param>
        /// <param name="lineNum">无需传入</param>
        public void Debug(string msg, [CallerMemberName] string methodName = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNum = 0)
        {
            LogPrint(Level.DebugLevel, msg, fileName, methodName, lineNum);
        }

        /// <summary>
        /// 记录普通级别的日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        /// <param name="methodName">无需传入</param>
        /// <param name="fileName">无需传入</param>
        /// <param name="lineNum">无需传入</param>
        public void Info(string msg, [CallerMemberName] string methodName = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNum = 0)
        {
            LogPrint(Level.InfoLevel, msg, fileName, methodName, lineNum);
        }

        /// <summary>
        /// 记录警告级别的日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        /// <param name="methodName">无需传入</param>
        /// <param name="fileName">无需传入</param>
        /// <param name="lineNum">无需传入</param>
        public void Warn(string msg, [CallerMemberName] string methodName = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNum = 0)
        {
            LogPrint(Level.WarnLevel, msg, fileName, methodName, lineNum);
        }

        /// <summary>
        /// 记录错误级别的日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        /// <param name="methodName">无需传入</param>
        /// <param name="fileName">无需传入</param>
        /// <param name="lineNum">无需传入</param>
        public void Error(string msg, [CallerMemberName] string methodName = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNum = 0)
        {
            LogPrint(Level.ErrorLevel, msg, fileName, methodName, lineNum);
        }

        /// <summary>
        /// 记录致命级别的日志
        /// </summary>
        /// <param name="msg">日志内容</param>
        /// <param name="methodName">无需传入</param>
        /// <param name="fileName">无需传入</param>
        /// <param name="lineNum">无需传入</param>
        public void Fatal(string msg, [CallerMemberName] string methodName = "", [CallerFilePath] string fileName = "", [CallerLineNumber] int lineNum = 0)
        {
            LogPrint(Level.FatalLevel, msg, fileName, methodName, lineNum);
        }
    }
}
