﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using YoYo.YuTooAiPb;

namespace YoYo.YuTooAiSDK
{
    /// <summary>
    /// 由图智能识别SDK接口封装
    /// </summary>
    public class AiGrpc
    {
        /// <summary>
        /// 日志记录器实例
        /// </summary>
        private readonly AiLogger log = new AiLogger();

        /// <summary>
        /// AI进程管理实例
        /// </summary>
        private readonly AiProcess aiProcess = new AiProcess();

        /// <summary>
        /// AI服务（启动/初始化/释放）线程互斥锁
        /// </summary>
        private static readonly Mutex aiRunInitAndUnInitMutex = new Mutex();

        /// <summary>
        /// gRPC管道未连接异常
        /// </summary>
        private static readonly PublicReply notConnectErr = new PublicReply
        {
            Code = ReplyCodeType.ErrUnknown,
            Msg = "通讯管道未连接"
        };

        /// <summary>
        /// 参数为空异常
        /// </summary>
        private static readonly PublicReply paramEmptyErr = new PublicReply
        {
            Code = ReplyCodeType.ErrParams,
            Msg = "参数不能为空"
        };

        /// <summary>
        /// 是否允许重启时调用初始化及相关接口
        /// </summary>
        private volatile bool rebootCallInit = false;

        /// <summary>
        /// 获取商品数据的托管方法）
        /// </summary>
        /// <returns>商品数据</returns>
        public delegate PluSyncRequest GetPluDataFunc();

        /// <summary>
        /// AI识别（流式）识别结果托管方法类型
        /// </summary>
        /// <param name="reply">识别结果</param>
        /// <returns>返回false时将会断开识别流接口</returns>
        public delegate bool AiMatchingReplyFunc(AiMatchingReply reply);

        /// <summary>
        /// AI识别（流式）线程
        /// </summary>
        private Task aiMatchingStreamThread;

        /// <summary>
        /// AI识别（流式）线程取消Token
        /// </summary>
        private CancellationTokenSource aiMatchingStreamThreadCancel;

        /// <summary>
        /// AI识别（流式）线程互斥锁
        /// </summary>
        private static readonly Mutex aiMatchingStreamThreadMutex = new Mutex();

        /// <summary>
        /// 相机取流（流式）响应结果托管方法类型
        /// </summary>
        /// <param name="reply">相机JPEG流</param>
        /// <returns>返回false时将会断开取流接口</returns>
        public delegate bool GetCameraStreamReplyFunc(GetCameraStreamReply reply);

        /// <summary>
        /// 相机取流（流式）线程
        /// </summary>
        private Task getCameraStreamThread;

        /// <summary>
        ///相机取流（流式）线程取消Token
        /// </summary>
        private CancellationTokenSource getCameraStreamThreadCancel;

        /// <summary>
        /// 相机取流（流式）线程互斥锁
        /// </summary>
        private static readonly Mutex getCameraStreamThreadMutex = new Mutex();

        /// <summary>
        /// SDK构造函数
        /// </summary>
        /// <param name="logPrint"></param>
        public AiGrpc(AiLogger.Print logPrint = null)
        {
            // 赋值日志记录器
            log.SetPrint(logPrint);
            // 覆盖默认的AiProcess实例
            aiProcess = new AiProcess(logPrint);
        }

        /// <summary>
        /// 运行AI识别进程
        /// </summary>
        /// <param name="vendorCode">厂商标识</param>
        /// <param name="vendorKey">厂商密钥</param>
        /// <param name="getPluData">AI进程崩溃后将会自动重启，并通过该托管函数自动同步一次商品数据</param>
        /// <param name="workPath">SDK工作目录</param>
        public PublicReply Run(string vendorCode, string vendorKey, GetPluDataFunc getPluData, string workPath = ".")
        {
            // 加锁
            bool ok = aiRunInitAndUnInitMutex.WaitOne(5);
            if (!ok)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = "禁止在服务(初始化/释放)期间启动服务"
                };
            }
            try
            {
                // 判断入参
                vendorCode = vendorCode.Trim();
                if (string.IsNullOrEmpty(vendorCode))
                {
                    return new PublicReply
                    {
                        Code = ReplyCodeType.ErrParams,
                        Msg = "厂商标识不能为空"
                    };
                }

                // 判断入参
                vendorKey = vendorKey.Trim();
                if (string.IsNullOrEmpty(vendorKey) || vendorKey.Length != 32)
                {
                    return new PublicReply
                    {
                        Code = ReplyCodeType.ErrParams,
                        Msg = "厂商密钥不能为空"
                    };
                }

                // 判断入参
                if (getPluData == null)
                {
                    return new PublicReply
                    {
                        Code = ReplyCodeType.ErrParams,
                        Msg = "获取商品数据的托管函数不能为空"
                    };
                }

                // 绑定gRPC连接成功后的事件
                aiProcess.GrpcConnectedEvent = () =>
                {
                    // 判断是否允许调用初始化及相关接口
                    if (rebootCallInit)
                    {
                        // 重新初始化
                        log.Info("AI进程崩溃，重新初始化服务中···");
                        PublicReply reply = Init();
                        if (reply == null)
                        {
                            log.Error("AI进程崩溃，重新初始化服务失败");
                            return;
                        }
                        if (reply.Code != ReplyCodeType.Success)
                        {
                            log.Error($"AI进程崩溃，重新初始化服务失败，${reply.Msg}");
                            return;
                        }
                        log.Info("AI进程崩溃，重新初始化服务成功");

                        // 获取商品数据重新传秤
                        log.Info("AI进程崩溃，重新传秤中···");
                        if (getPluData == null)
                        {
                            log.Error("AI进程崩溃，重新传秤失败，获取商品数据的托管函数为空，请手动同步商品数据");
                            return;
                        }
                        reply = PluSync(getPluData());
                        if (reply == null)
                        {
                            log.Error("AI进程崩溃，重新传秤失败，请手动同步商品数据");
                            return;
                        }
                        if (reply.Code != ReplyCodeType.Success)
                        {
                            log.Error($"AI进程崩溃，重新传秤失败，请手动同步商品数据，{reply.Msg}");
                            return;
                        }
                        log.Info("AI进程崩溃，重新传秤成功");
                    }
                };

                // 绑定AI进程退出事件
                aiProcess.ExitEvent = () =>
                {
                    // AI Process退出了
                };

                // 绑定接收到系统关机的事件
                aiProcess.SystemShutdownEvent = () =>
                {
                    // 赶紧保存数据
                    log.Info("接收到关机事件，正在紧急保存数据中···");
                    PublicReply reply = UnInit();
                    if (reply == null)
                    {
                        log.Info("接收到关机事件，紧急保存数据失败");
                        return;
                    }
                    if (reply.Code != ReplyCodeType.Success)
                    {
                        log.Info($"接收到关机事件，紧急保存数据失败，${reply.Msg}");
                        return;
                    }
                    log.Info("接收到关机事件，紧急保存数据成功");
                };

                // 运行进程
                ok = aiProcess.Run(vendorCode, vendorKey, workPath);
                if (ok)
                {
                    // 已启动守护进程
                    return new PublicReply
                    {
                        Code = ReplyCodeType.Success,
                        Msg = "AI识别进程启动成功"
                    };
                }
                // 已启动守护进程
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = "AI识别进程启动失败"
                };
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message);
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = ex.Message
                };
            }
            finally
            {
                // 释放锁
                aiRunInitAndUnInitMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// 获取AI进程当前使用的端口
        /// </summary>
        /// <returns>gRPC和HTTP服务端口</returns>
        public int GetAiProcessPort()
        {
            return aiProcess.AiProcessPort;
        }

        /// <summary>
        /// GetVersion 获取版本信息
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口在初始化之前可调用<br/>
        /// 2. 初始化之前调用该接口无法获取到识别算法的版本信息
        /// </remarks>
        /// <returns>版本信息</returns>
        public GetVersionReply GetVersion()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new GetVersionReply
                    {
                        Status = notConnectErr
                    };
                }
                // 调用版本信息接口
                return publicClient.GetVersion(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new GetVersionReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// Init 服务初始化接口
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 建议返回状态成功后不再调用本接口
        /// </remarks>
        /// <returns>初始化结果（未激活时会返回未激活状态）</returns>
        public PublicReply Init()
        {
            // 加锁
            bool ok = aiRunInitAndUnInitMutex.WaitOne(5);
            if (!ok)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = "禁止在服务(启动/释放)期间初始化服务"
                };
            }
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 调用初始化接口
                PublicReply reply = publicClient.Init(new EmptyRequest());
                if (reply != null && reply.Code == ReplyCodeType.Success)
                {
                    // 标记允许重启时调用初始化及相关接口
                    rebootCallInit = true;
                }
                // 响应结果
                return reply;
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
            finally
            {
                // 释放锁
                aiRunInitAndUnInitMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// UnInit 服务释放接口（保存学习数据）
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 程序退出前务必调用一次该接口，否则部分学习数据可能丢失
        /// </remarks>
        /// <returns>释放结果</returns>
        public PublicReply UnInit()
        {
            // 加锁
            bool ok = aiRunInitAndUnInitMutex.WaitOne(5);
            if (!ok)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = "禁止在服务(启动/初始化)期间释放服务"
                };
            }
            try
            {
                // 重置状态
                rebootCallInit = false;
                // 尝试结束AI识别(流式)线程
                aiMatchingStreamThreadCancel?.Cancel();
                // 尝试等待AI识别(流式)线程结束
                aiMatchingStreamThread?.Wait();
                // 尝试结束相机取流(流式)线程
                getCameraStreamThreadCancel?.Cancel();
                // 尝试等待相机取流(流式)线程结束
                getCameraStreamThread?.Wait();
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行资源释放
                PublicReply reply = publicClient.UnInit(new EmptyRequest());
                // 尝试通知守护线程停止监听
                aiProcess?.Exit();
                // 响应结果
                return reply;
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
            finally
            {
                // 释放锁
                aiRunInitAndUnInitMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// Activate 设备激活
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 建议仅在Init接口返回未激活状态码时调用该接口
        /// </remarks>
        /// <param name="activateRequest">激活参数</param>
        /// <returns>激活结果</returns>
        public PublicReply Activate(ActivateRequest activateRequest)
        {
            try
            {
                // 判断激活信息
                if (activateRequest == null)
                {
                    return paramEmptyErr;
                }

                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行激活
                return publicClient.Activate(activateRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// PluSync 商品同步
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 务必在Init接口或Activate接口调用成功后同步一次商品数据<br/>
        /// 2. 商品数据同步为全量同步，请一次同步完所有商品
        /// </remarks>
        /// <param name="pluSyncRequest">商品数据</param>
        /// <returns>同步结果</returns>
        public PublicReply PluSync(PluSyncRequest pluSyncRequest)
        {
            try
            {
                // 校验商品数据
                if (pluSyncRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行商品同步
                return publicClient.PluSync(pluSyncRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// GetPerformanceMode 获取性能模式
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口为获取当前算法使用的性能模式
        /// </remarks>
        /// <returns>当前性能模式</returns>
        public GetPerformanceModeReply GetPerformanceMode()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new GetPerformanceModeReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行性能模式获取
                return publicClient.GetPerformanceMode(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new GetPerformanceModeReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// SetPerformanceMode 设置性能模式
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口为配置当前算法使用的性能模式<br/>
        /// 2. 当识别功能正常时不建议修改性能模式
        /// </remarks>
        /// <param name="setPerformanceModeRequest">性能模式配置参数</param>
        /// <returns>配置结果</returns>
        public PublicReply SetPerformanceMode(SetPerformanceModeRequest setPerformanceModeRequest)
        {
            try
            {
                // 校验数据
                if (setPerformanceModeRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行性能配置
                return publicClient.SetPerformanceMode(setPerformanceModeRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// ShowCameraAreaView 渲染相机识别区域视图
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口已将GetCameraList，OpenCamera，GetCameraArea，SetCameraArea四个接口封装成内置UI<br/>
        /// 2. 调用该接口将会通过弹窗方式打开UI
        /// </remarks>
        /// <returns>是否打开成功</returns>
        public PublicReply ShowCameraAreaView()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行识别区域配置
                return publicClient.ShowCameraAreaView(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// GetCameraList 获取相机列表
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 调用GetCameraList获取相机列表以及正在使用的相机<br/>
        /// 2. 调用OpenCamera接口打开相机列表中的某个相机
        /// </remarks>
        /// <returns>相机列表</returns>
        public GetCameraListReply GetCameraList()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new GetCameraListReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行相机列表获取
                return publicClient.GetCameraList(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new GetCameraListReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// OpenCamera 打开相机
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 调用GetCameraList获取相机列表以及正在使用的相机<br/>
        /// 2. 调用OpenCamera接口打开相机列表中的某个相机
        /// </remarks>
        /// <param name="openCameraRequest">相机打开参数</param>
        /// <returns>是否打开成功</returns>
        public PublicReply OpenCamera(OpenCameraRequest openCameraRequest)
        {
            try
            {
                // 校验数据
                if (openCameraRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行相机打开
                return publicClient.OpenCamera(openCameraRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// GetCameraArea 获取识别区域
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 调用GetCameraArea接口获取已存在的相机区域坐标<br/>
        /// 2. 调用GetCameraImage或GetCameraStream接口渲染图像<br/>
        /// 3. 在图像上显示区域坐标，注意渲染比例和真实比例的换算<br/>
        /// 4. 调整合适的识别区域，将渲染比例换算为真实比例<br/>
        /// 5. 调用SetCameraArea接口，将真实比例传入进行配置
        /// </remarks>
        /// <returns>相机识别区域</returns>
        public GetCameraAreaReply GetCameraArea()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new GetCameraAreaReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行相机识别区域获取
                return publicClient.GetCameraArea(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new GetCameraAreaReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// SetCameraArea 配置识别区域
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 调用GetCameraArea接口获取已存在的相机区域坐标<br/>
        /// 2. 调用GetCameraImage或GetCameraStream接口渲染图像<br/>
        /// 3. 在图像上显示区域坐标，注意渲染比例和真实比例的换算<br/>
        /// 4. 调整合适的识别区域，将渲染比例换算为真实比例<br/>
        /// 5. 调用SetCameraArea接口，将真实比例传入进行配置
        /// </remarks>
        /// <param name="setCameraAreaRequest">新的识别区域绝对坐标</param>
        /// <returns></returns>
        public PublicReply SetCameraArea(SetCameraAreaRequest setCameraAreaRequest)
        {
            try
            {
                // 校验数据
                if (setCameraAreaRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行识别区域配置
                return publicClient.SetCameraArea(setCameraAreaRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// GetDeviceInfo 获取设备信息
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口能获取到设备和门店的基本信息
        /// </remarks>
        /// <returns>设备信息及调用状态</returns>
        public GetDeviceInfoReply GetDeviceInfo()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new GetDeviceInfoReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行设备信息获取
                return publicClient.GetDeviceInfo(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new GetDeviceInfoReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// EditShopInfo 编辑门店信息
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口能修改门店的基本信息
        /// </remarks>
        /// <param name="editShopInfoRequest">新的门店信息</param>
        /// <returns>门店信息修改结果</returns>
        public PublicReply EditShopInfo(EditShopInfoRequest editShopInfoRequest)
        {
            try
            {
                // 校验数据
                if (editShopInfoRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行门店信息修改
                return publicClient.EditShopInfo(editShopInfoRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// GetCameraImage 获取相机图片（JPEG）
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口为GetCameraStream接口的一元版本<br/>
        /// 2. 如果对流式接口对接不便或不了解，建议使用该接口
        /// </remarks>
        /// <returns>JPEG图像和状态</returns>
        public GetCameraStreamReply GetCameraImage()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new GetCameraStreamReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行JPEG图像获取
                return publicClient.GetCameraImage(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new GetCameraStreamReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// GetCameraStream 获取相机流（JPEG）【流式】
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口为流式接口<br/>
        /// 2. 如果对流式接口对接不便或不了解，建议使用GetCameraImage接口
        /// </remarks>
        /// <param name="getCameraStreamRequest">相机流请求参数</param>
        /// <param name="replyFunc">相机流回调方法，该方法返回false将会断开取流</param>
        /// <returns>相机取流打开状态</returns>
        public PublicReply GetCameraStream(GetCameraStreamRequest getCameraStreamRequest, GetCameraStreamReplyFunc replyFunc)
        {
            // 加锁
            if (!getCameraStreamThreadMutex.WaitOne())
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = "已有一个操作正在打开相机流，禁止重复打开"
                };
            }
            try
            {
                // 检查参数
                if (getCameraStreamRequest == null || replyFunc == null)
                {
                    return notConnectErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 尝试取消上一个线程
                getCameraStreamThreadCancel?.Cancel();
                // 尝试等待上一个线程结束
                getCameraStreamThread?.Wait();
                // 创建一个取消签名
                getCameraStreamThreadCancel = new CancellationTokenSource();
                // 执行JPEG图像获取
                AsyncServerStreamingCall<GetCameraStreamReply> stream = publicClient.GetCameraStream(getCameraStreamRequest);
                // 创建新的线程
                getCameraStreamThread = Task.Run(async () =>
                {
                    try
                    {
                        try
                        {
                            while (await stream.ResponseStream.MoveNext(getCameraStreamThreadCancel.Token) && replyFunc != null)
                            {
                                if (!replyFunc(stream.ResponseStream.Current))
                                {
                                    return;
                                }
                            }
                        }
                        catch (RpcException ex)
                        {
                            // 排除识别流取消异常
                            if (!ex.StatusCode.Equals(StatusCode.Cancelled))
                            {
                                // 记录异常日志
                                log.Fatal(ex.Status.Detail);
                            }
                        }
                        catch (Exception e)
                        {
                            log.Fatal(e.Message);
                        }
                        finally
                        {
                            // 释放流
                            stream.Dispose();
                            // 标记相机取流取消了
                            getCameraStreamThreadCancel?.Cancel();
                        }
                    }
                    catch (Exception ex)
                    {
                        // 捕获finally异常
                        log.Fatal(ex.Message);
                    }
                });
                // 返回相机取流打开成功
                return new PublicReply
                {
                    Code = ReplyCodeType.Success,
                    Msg = "相机取流打开成功"
                };
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
            finally
            {
                // 释放锁
                getCameraStreamThreadMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// PluMatch 商品信息匹配
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口为批量匹配商品信息接口，并非识别接口<br/>
        /// 2. 为考虑性能，建议500~1000个商品传入该接口进行匹配
        /// </remarks>
        /// <param name="pluMatchRequest">需要匹配的商品信息</param>
        /// <returns>商品信息匹配结果、ICON等</returns>
        public PluMatchReply PluMatch(PluMatchRequest pluMatchRequest)
        {
            try
            {
                // 校验数据
                if (pluMatchRequest == null)
                {
                    return new PluMatchReply
                    {
                        Status = paramEmptyErr
                    };
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new PluMatchReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行JPEG图像获取
                return publicClient.PluMatch(pluMatchRequest);
            }
            catch (Exception e)
            {
                return new PluMatchReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// GetStudyData 获取学习数据
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 在当前机器上调用GetStudyData接口获取学习数据<br/>
        /// 2. 将通过GetStudyData接口获取到的学习数据传输到其他机器<br/>
        /// 3. 将收到的学习数据传入SetStudyData接口，通过指定模式操作学习数据
        /// </remarks>
        /// <returns>学习数据和状态结果</returns>
        public GetStudyDataReply GetStudyData(GetStudyDataRequest getStudyDataRequest)
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new GetStudyDataReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行学习数据获取
                return publicClient.GetStudyData(getStudyDataRequest);
            }
            catch (Exception e)
            {
                return new GetStudyDataReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// SetStudyData 配置学习数据
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 在其它机器上调用GetStudyData接口获取学习数据<br/>
        /// 2. 将在其他机器上通过GetStudyData接口获取到的学习数据传入本机<br/>
        /// 3. 将收到的学习数据传入SetStudyData接口，通过指定模式操作学习数据
        /// </remarks>
        /// <param name="data"></param>
        /// <returns>状态结果</returns>
        public PublicReply SetStudyData(SetStudyDataRequest data)
        {
            try
            {
                // 判断参数
                if (data == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行学习数据配置
                return publicClient.SetStudyData(data);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// AI识别（AiMatching和AiMatchingStream仅允许使用其中一个）
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 先调用AiMatching或AiMatchingStream接口获取识别结果<br/>
        /// 2. 如果对流式接口对接不便或不了解，建议使用该接口<br/>
        /// 3. 调用AiMark接口对识别结果进行调校
        /// </remarks>
        /// <param name="aiMatchingRequest">识别参数</param>
        /// <returns>识别结果</returns>
        public AiMatchingReply AiMatching(AiMatchingRequest aiMatchingRequest)
        {
            try
            {
                // 判断参数
                if (aiMatchingRequest == null)
                {
                    return new AiMatchingReply
                    {
                        Status = paramEmptyErr
                    };
                }
                // 是否已启用了流式识别
                if (aiMatchingStreamThread != null)
                {
                    return new AiMatchingReply
                    {
                        Status = new PublicReply
                        {
                            Code = ReplyCodeType.ErrUnknown,
                            Msg = "已启用AI识别（流式）接口，禁止再使用一元接口"
                        }
                    };
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new AiMatchingReply
                    {
                        Status = notConnectErr
                    };
                }                // 执行识别
                return publicClient.AiMatching(aiMatchingRequest);
            }
            catch (Exception e)
            {
                return new AiMatchingReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// AI识别（流式）线程
        /// </summary>
        /// <param name="requestQueue">识别参数队列</param>
        /// <param name="replyFunc">识别结果回调</param>
        /// <returns>识别线程</returns>
        private Task AiMatchingStreamThread(BlockingCollection<AiMatchingRequest> requestQueue, AiMatchingReplyFunc replyFunc)
        {
            // 返回一个新的识别线程
            return Task.Run(() =>
            {
                // 创建AI识别（流式）线程取消凭证
                aiMatchingStreamThreadCancel = new CancellationTokenSource();

                // 死循环监听：直到外部通知取消监听
                while (!aiMatchingStreamThreadCancel.IsCancellationRequested)
                {
                    try
                    {
                        // 获取客户端
                        YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                        // 检查客户端是否存在
                        if (publicClient == null)
                        {
                            log.Fatal(notConnectErr.Msg);
                            Thread.Sleep(1500);
                            continue;
                        }

                        // 创建识别流取消凭证
                        CancellationTokenSource internalCancel = CancellationTokenSource.CreateLinkedTokenSource(aiMatchingStreamThreadCancel.Token);
                        // 打开识别流通道
                        AsyncDuplexStreamingCall<AiMatchingRequest, AiMatchingReply> stream = publicClient.AiMatchingStream(cancellationToken: internalCancel.Token);

                        //------------------------------ 异步读取识别结果 ------------------------------//
                        Task.Run(async () =>
                        {
                            try
                            {
                                // 死循环读取识别结果：直到 识别流被取消 或 读取到流结束
                                while (await stream.ResponseStream.MoveNext(internalCancel.Token) && replyFunc != null)
                                {
                                    // 获取当前结果，回调委托方法
                                    if (!replyFunc(stream.ResponseStream.Current))
                                    {
                                        // 用户结束了识别流
                                        aiMatchingStreamThreadCancel?.Cancel();
                                        return;
                                    }
                                }
                            }
                            catch (RpcException ex)
                            {
                                // 排除识别流取消异常
                                if (!ex.StatusCode.Equals(StatusCode.Cancelled))
                                {
                                    // 记录异常日志
                                    log.Fatal(ex.Status.Detail);
                                }
                            }
                            catch (Exception ex)
                            {
                                // 记录异常日志
                                log.Fatal(ex.Message);
                            }
                            finally
                            {
                                // 标记识别流为已取消
                                internalCancel?.Cancel();
                            }
                        });

                        //------------------------------ 死循环读取识别参数队列 ------------------------------//
                        try
                        {
                            // 死循环读取队列，直到识别流被取消
                            while (!internalCancel.IsCancellationRequested)
                            {
                                // 读取一条请求参数
                                AiMatchingRequest request = requestQueue.Take(internalCancel.Token);
                                // 发送到gRPC进行识别
                                stream.RequestStream.WriteAsync(request).Wait();
                            }
                        }
                        catch (OperationCanceledException)
                        {
                            // 排除识别流取消异常
                        }
                        catch (Exception ex)
                        {
                            // 记录异常日志
                            log.Fatal(ex.Message);
                        }
                        finally
                        {
                            // 标记识别流为已取消
                            internalCancel?.Cancel();
                        }

                        //------------------------------ 出现异常了，需要重新打开流 ------------------------------//
                        // 尝试释放资源
                        stream.Dispose();
                        // AI识别（流式）线程未被取消时将会重新打开流
                        if (!aiMatchingStreamThreadCancel.IsCancellationRequested)
                        {
                            log.Info("AI识别流重新打开");
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Fatal(ex.Message);
                        // AI识别（流式）线程未被取消时将会重新打开流
                        if (!aiMatchingStreamThreadCancel.IsCancellationRequested)
                        {
                            log.Info("AI识别（流式）线程重新启动");
                        }
                    }
                }
                log.Info("AI识别（流式）线程结束");
            });
        }

        /// <summary>
        /// AI识别【流式】（AiMatching和AiMatchingStream仅允许使用其中一个）
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 先调用AiMatching或AiMatchingStream接口获取识别结果<br/>
        /// 2. 如果对流式接口对接不便或不了解，建议使用AiMatching接口<br/>
        /// 3. 调用AiMark接口对识别结果进行调校
        /// </remarks>
        /// <param name="requestQueue">识别参数队列</param>
        /// <param name="replyFunc">识别结果回调</param>
        /// <returns>AI识别线程是否启动成功</returns>
        public PublicReply AiMatchingStream(BlockingCollection<AiMatchingRequest> requestQueue, AiMatchingReplyFunc replyFunc)
        {
            // 判断参数
            if (requestQueue == null || replyFunc == null)
            {
                return paramEmptyErr;
            }
            // 不要阻塞主线程
            Task.Run(() =>
            {
                // 加锁
                aiMatchingStreamThreadMutex.WaitOne();
                try
                {
                    // 仅允许一个识别（流式）线程存在，取消上一个AI识别（流式）线程
                    aiMatchingStreamThreadCancel?.Cancel();
                    // 等到上一个AI识别（流式）线程结束
                    aiMatchingStreamThread?.Wait();
                    // 创建一个新的AI识别（流式）线程
                    aiMatchingStreamThread = AiMatchingStreamThread(requestQueue, replyFunc);
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }
                finally
                {
                    // 释放锁
                    aiMatchingStreamThreadMutex.ReleaseMutex();
                }
            });
            // 成功
            return new PublicReply
            {
                Code = ReplyCodeType.Success,
                Msg = "启动AI识别（流式）线程成功"
            };
        }

        /// <summary>
        /// AiMark 点选标记
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 先调用AiMatching或AiMatchingStream接口获取识别结果<br/>
        /// 2. 调用AiMark接口对识别结果进行调校
        /// </remarks>
        /// <param name="aiMarkRequest">标记参数</param>
        /// <returns>标记结果</returns>
        public PublicReply AiMark(AiMarkRequest aiMarkRequest)
        {
            try
            {
                // 判断参数
                if (aiMarkRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行标记
                return publicClient.AiMark(aiMarkRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// StudyModeMatching 学习模式-识别
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 先调用StudyModeMatching接口获取识别ID<br/>
        /// 2. 将需要保存的识别ID传入StudyModeMark接口进行学习数据保存
        /// </remarks>
        /// <returns>识别ID和识别图像以及状态</returns>
        public StudyModeMatchingReply StudyModeMatching()
        {
            try
            {
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return new StudyModeMatchingReply
                    {
                        Status = notConnectErr
                    };
                }
                // 执行学习模式识别
                return publicClient.StudyModeMatching(new EmptyRequest());
            }
            catch (Exception e)
            {
                return new StudyModeMatchingReply
                {
                    Status = new PublicReply
                    {
                        Code = ReplyCodeType.ErrUnknown,
                        Msg = e.Message
                    }
                };
            }
        }

        /// <summary>
        /// StudyModeMark 学习模式-保存
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 先调用StudyModeMatching接口获取识别ID<br/>
        /// 2. 将需要保存的识别ID传入StudyModeMark接口进行学习数据保存
        /// </remarks>
        /// <param name="studyModeMarkRequest">学习模式标记参数</param>
        /// <returns>学习结果状态</returns>
        public PublicReply StudyModeMark(StudyModeMarkRequest studyModeMarkRequest)
        {
            try
            {
                // 判断参数
                if (studyModeMarkRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行学习模式标记
                return publicClient.StudyModeMark(studyModeMarkRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// CorrectStudyData 矫正学习数据
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 先调用AiMatching或AiMatchingStream接口获取识别结果<br/>
        /// 2. 果对流式接口对接不便或不了解，建议使用该接口<br/>
        /// 3. 将想移除的识别结果传入该接口进行学习数据矫正
        /// </remarks>
        /// <param name="correctStudyDataRequest">矫正参数</param>
        /// <returns>矫正结果状态</returns>
        public PublicReply CorrectStudyData(CorrectStudyDataRequest correctStudyDataRequest)
        {
            try
            {
                // 判断参数
                if (correctStudyDataRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行学习数据矫正
                return publicClient.CorrectStudyData(correctStudyDataRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// UpdatePluStatus 更新商品状态
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口会修改商品的状态信息，锁定和下架的商品将不会出现在识别结果中<br/>
        /// 2. 已下架的商品通过Mark后会自动上架
        /// </remarks>
        /// <param name="updatePluStatusRequest">需要更新的商品数据</param>
        /// <returns>更新结果状态</returns>
        public PublicReply UpdatePluStatus(UpdatePluStatusRequest updatePluStatusRequest)
        {
            try
            {
                // 判断参数
                if (updatePluStatusRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行商品状态更新
                return publicClient.UpdatePluStatus(updatePluStatusRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// DeletePluData 删除指定商品数据
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口会删除传入商品的所有数据，包含学习数据<br/>
        /// 2. 该接口入参仅需为商品编码字段赋值即可，商品名称字段无需赋值
        /// </remarks>
        /// <param name="deletePluDataRequest">需要删除的商品数据</param>
        /// <returns>删除结果状态</returns>
        public PublicReply DeletePluData(DeletePluDataRequest deletePluDataRequest)
        {
            try
            {
                // 判断参数
                if (deletePluDataRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行商品删除
                return publicClient.DeletePluData(deletePluDataRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }

        /// <summary>
        /// RemovePluStudyData 移除指定商品的学习数据
        /// </summary>
        /// <remarks>
        /// 注意事项:<br/>
        /// 1. 该接口会删除传入商品的学习数据，商品任然会保留<br/>
        /// 2. 该接口入参仅需为商品编码字段赋值即可，商品名称字段无需赋值
        /// </remarks>
        /// <param name="removePluStudyDataRequest">需要移除学习数据的商品</param>
        /// <returns>移除结果状态</returns>
        public PublicReply RemovePluStudyData(RemovePluStudyDataRequest removePluStudyDataRequest)
        {
            try
            {
                // 判断参数
                if (removePluStudyDataRequest == null)
                {
                    return paramEmptyErr;
                }
                // 获取客户端
                YoYoAiPublic.YoYoAiPublicClient publicClient = aiProcess.PublicClient;
                // 检查客户端是否存在
                if (publicClient == null)
                {
                    return notConnectErr;
                }
                // 执行商品的学习数据移除
                return publicClient.RemovePluStudyData(removePluStudyDataRequest);
            }
            catch (Exception e)
            {
                return new PublicReply
                {
                    Code = ReplyCodeType.ErrUnknown,
                    Msg = e.Message
                };
            }
        }
    }
}
