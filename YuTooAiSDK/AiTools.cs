﻿using System;
using System.Linq;
using System.Net.NetworkInformation;

namespace YoYo.YuTooAiSDK
{
    /// <summary>
    /// 工具类
    /// </summary>
    public class AiTools
    {
        /// <summary>
        /// 获取一个可用的随机端口
        /// </summary>
        /// <returns></returns>
        public static int GetRandomPort()
        {
            // 初始化随机数实例
            var random = new Random();
            // 限制随机数范围并生成随机数
            var randomPort = random.Next(10000, 45000);
            // 通过循环判断随机端口是否被占用
            while (IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners().Any(p => p.Port == randomPort))
            {
                // 端口被占用，重新生成随机端口
                randomPort = random.Next(10000, 45000);
            }
            // 返回生成好的随机端口
            return randomPort;
        }
    }
}
